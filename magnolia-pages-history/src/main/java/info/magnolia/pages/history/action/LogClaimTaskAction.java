/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.pages.history.action;

import info.magnolia.cms.security.SecuritySupport;
import info.magnolia.context.MgnlContext;
import info.magnolia.i18nsystem.SimpleTranslator;
import info.magnolia.jcr.util.NodeNameHelper;
import info.magnolia.pages.history.util.CommonUtil;
import info.magnolia.task.Task;
import info.magnolia.task.TasksManager;
import info.magnolia.ui.ValueContext;
import info.magnolia.ui.admincentral.shellapp.pulse.task.action.ClaimTaskAction;
import info.magnolia.ui.admincentral.shellapp.pulse.task.action.ClaimTaskActionDefinition;
import info.magnolia.ui.api.shell.Shell;
import info.magnolia.ui.framework.message.MessagesManager;

import javax.inject.Inject;
import javax.jcr.Node;

/**
 * Log based on ClaimTaskAction.
 */
public class LogClaimTaskAction extends ClaimTaskAction {

    private final NodeNameHelper nodeNameHelper;

    @Inject
    public LogClaimTaskAction(ClaimTaskActionDefinition definition,
                              TasksManager tasksManager,
                              Shell shell,
                              MessagesManager messagesManager,
                              SimpleTranslator i18n,
                              SecuritySupport securitySupport,
                              ValueContext<Task> valueContext,
                              NodeNameHelper nodeNameHelper) {
        super(definition, tasksManager, shell, messagesManager, i18n, securitySupport, valueContext);
        this.nodeNameHelper = nodeNameHelper;
    }

    @Override
    protected void executeTask(TasksManager taskManager, Task task) {
        final String userId = MgnlContext.getUser().getName();
        Node content = CommonUtil.getSelectedFromTaskContent(task.getContent());
        if (content != null) {
            CommonUtil.storeTasksHistory(nodeNameHelper,
                    content,
                    "ASSIGN TO ME",
                    userId);
        }
        super.executeTask(taskManager, task);
    }
}
