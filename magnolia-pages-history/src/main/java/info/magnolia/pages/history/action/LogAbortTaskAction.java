/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.pages.history.action;

import info.magnolia.context.Context;
import info.magnolia.jcr.util.NodeNameHelper;
import info.magnolia.pages.history.util.CommonUtil;
import info.magnolia.task.Task;
import info.magnolia.task.TasksManager;
import info.magnolia.task.schedule.TaskSchedulerService;
import info.magnolia.ui.ValueContext;
import info.magnolia.ui.admincentral.shellapp.pulse.task.action.AbortTaskAction;
import info.magnolia.ui.admincentral.shellapp.pulse.task.action.ResolveTaskActionDefinition;
import info.magnolia.ui.api.app.SubAppContext;
import info.magnolia.ui.api.shell.Shell;

import javax.inject.Inject;
import javax.jcr.Node;

/**
 * Log based on AbortTaskAction.
 */
public class LogAbortTaskAction extends AbortTaskAction {

    private final Context context;
    private final NodeNameHelper nodeNameHelper;
    private final SubAppContext subAppContext;

    @Inject
    public LogAbortTaskAction(ResolveTaskActionDefinition definition,
                              TasksManager taskManager,
                              Shell shell,
                              TaskSchedulerService schedulerService,
                              ValueContext<Task> valueContext,
                              Context context,
                              NodeNameHelper nodeNameHelper,
                              SubAppContext subAppContext) {
        super(definition, taskManager, shell, schedulerService, valueContext);
        this.context = context;
        this.nodeNameHelper = nodeNameHelper;
        this.subAppContext = subAppContext;
    }

    @Override
    protected void executeTask(TasksManager taskManager, Task task) {
        String userId = this.context.getUser().getName();
        Node content = CommonUtil.getSelectedFromTaskContent(task.getContent());
        if (content != null) {
            CommonUtil.storeTasksHistory(nodeNameHelper,
                    content,
                    "ABORT",
                    userId);
        }
        super.executeTask(taskManager, task);
        // Show Notification
        subAppContext.getAppContext().sendLocalMessage(CommonUtil.createMessage(task.getContent(), "aborted"));
    }
}
