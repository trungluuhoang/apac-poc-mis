/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.pages.history.util;

import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.NodeNameHelper;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.ui.api.message.Message;
import info.magnolia.ui.api.message.MessageType;

import java.util.Calendar;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Util that uses to common functions.
 */
public class CommonUtil {

    private static final Logger log = LoggerFactory.getLogger(CommonUtil.class);

    public static void storeTasksHistory(NodeNameHelper nodeNameHelper,
                                         Node current,
                                         String action,
                                         String user) {
        try {
            Node root = MgnlContext.getJCRSession("tasks-history").getRootNode();
            Node tasksLog = root.addNode(nodeNameHelper
                    .getValidatedName(nodeNameHelper.getUniqueName(root, "log")), NodeTypes.Content.NAME);
            tasksLog.setProperty("nodePath", current.getPath());
            tasksLog.setProperty("workspace", current.getSession().getWorkspace().getName());
            tasksLog.setProperty("action", action);
            tasksLog.setProperty("pageLastModifiedBy", user);
            tasksLog.setProperty("pageLastModifiedDate", Calendar.getInstance());
            tasksLog.getSession().save();
        } catch (RepositoryException e) {
            log.error("An error occurs during store page tasks history.", e.getMessage());
        }
    }

    public static Node getSelectedFromTaskContent(Map<String, Object> content) {
        try {
            String uuid = String.valueOf(content.get("uuid"));
            String workspace = String.valueOf(content.get("repository"));
            Session session = MgnlContext.getJCRSession(workspace);
            return session.getNodeByIdentifier(uuid);
        } catch (RepositoryException e) {
            log.error("An error occurs during get selected page.", e.getMessage());
        }
        return null;
    }

    public static Message createMessage(Map<String, Object> content, String type) {
        final Message message = new Message();
        String nodeType = getNodeType(content.get("repository").toString(), content.get("uuid").toString());
        message.setSubject("Publication request " + type + " for " + nodeType + " " + content.get("path"));
        message.setMessage("The publication of " + content.get("path") + " has been " + type);
        message.setType(MessageType.WORKITEM);
        return message;
    }

    private static String getNodeType(String repository, String uuid) {
        try {
            return MgnlContext.getJCRSession(repository).getNodeByIdentifier(uuid).getPrimaryNodeType().getName();
        } catch (RepositoryException e) {
            log.error("An error occurs during get node type.", e.getMessage());
            return "";
        }
    }

    public static String getClientIp(HttpServletRequest request) {
        if (request != null) {
            String remoteAddr = checkIpAddrFromHeader(request);
            return StringUtils.isBlank(remoteAddr) ? request.getRemoteAddr() : remoteAddr;
        }
        return "";
    }

    private static String checkIpAddrFromHeader(HttpServletRequest request) {
        String ip = request.getHeader("X-FORWARDED-FOR");

        if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }

        if (StringUtils.isBlank(ip)  || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (StringUtils.isBlank(ip)  || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }

        if (StringUtils.isBlank(ip)  || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }

        return ip;
    }

}
