/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.pages.history.action;

import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.NodeNameHelper;
import info.magnolia.pages.history.util.CommonUtil;
import info.magnolia.task.Task;
import info.magnolia.task.TasksManager;
import info.magnolia.ui.ValueContext;
import info.magnolia.ui.admincentral.shellapp.pulse.task.action.RetryTaskAction;
import info.magnolia.ui.admincentral.shellapp.pulse.task.action.RetryTaskActionDefinition;
import info.magnolia.ui.api.app.SubAppContext;
import info.magnolia.ui.api.shell.Shell;

import javax.inject.Inject;
import javax.jcr.Node;

/**
 * Log based on RetryTaskAction.
 */
public class LogRetryTaskAction extends RetryTaskAction {

    private final NodeNameHelper nodeNameHelper;
    private final SubAppContext subAppContext;

    @Inject
    public LogRetryTaskAction(RetryTaskActionDefinition definition,
                              TasksManager tasksManager,
                              Shell shell,
                              ValueContext<Task> valueContext,
                              NodeNameHelper nodeNameHelper,
                              SubAppContext subAppContext) {
        super(definition, tasksManager, shell, valueContext);
        this.nodeNameHelper = nodeNameHelper;
        this.subAppContext = subAppContext;
    }

    @Override
    protected void executeTask(TasksManager tasksManager, Task task) {
        final String userId = MgnlContext.getUser().getName();
        Node content = CommonUtil.getSelectedFromTaskContent(task.getContent());
        if (content != null) {
            CommonUtil.storeTasksHistory(nodeNameHelper,
                    content,
                    "RETRY",
                    userId);
        }
        super.executeTask(tasksManager, task);
        // Show Notification
        subAppContext.getAppContext().sendLocalMessage(CommonUtil.createMessage(task.getContent(), "retried"));
    }
}
