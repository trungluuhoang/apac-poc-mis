/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.pages.history.action;

import info.magnolia.rendering.engine.RenderingEngine;
import info.magnolia.rendering.template.registry.TemplateDefinitionRegistry;
import info.magnolia.ui.ValueContext;
import info.magnolia.ui.api.action.AbstractAction;
import info.magnolia.ui.chooser.definition.ChooserDefinition;
import info.magnolia.ui.contentapp.action.CloseActionDefinition;
import info.magnolia.ui.dialog.layout.DefaultEditorActionLayoutDefinition;
import info.magnolia.ui.framework.overlay.ChooserController;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.jcr.Node;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Lists;
import com.mailslurp.apis.InboxControllerApi;
import com.mailslurp.clients.ApiClient;
import com.mailslurp.clients.Configuration;
import com.mailslurp.models.Inbox;
import com.mailslurp.models.SendEmailOptions;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;

import java.io.IOException;

import lombok.SneakyThrows;
import okhttp3.OkHttpClient;

/**
 * Show page logs action.
 */
public class ShowPageLogsAction extends AbstractAction<ShowPageLogsActionDefinition> {

    private final ValueContext<Node> valueContext;
    private final ChooserController chooserController;
    private final RenderingEngine renderingEngine;
    private final TemplateDefinitionRegistry templateDefinitionRegistry;

    private static ApiClient apiClient;
    private static final Long TIMEOUT = 30000L;
    private static final String apiKey = "1ace15f8ca1343f78ddedc1daa945c9eca64a7b795ff37301613de91360321b1";

    @Inject
    ShowPageLogsAction(ShowPageLogsActionDefinition definition,
                       ValueContext<Node> valueContext,
                       ChooserController chooserController,
                       RenderingEngine renderingEngine,
                       TemplateDefinitionRegistry templateDefinitionRegistry) {
        super(definition);
        this.valueContext = valueContext;
        this.chooserController = chooserController;
        this.renderingEngine = renderingEngine;
        this.templateDefinitionRegistry = templateDefinitionRegistry;
    }

    @Override
    public void execute() {
        valueContext.getSingle().ifPresent(this::executeForNode);
    }

    @SneakyThrows
    private void executeForNode(Node node) {
        ChooserDefinition<String, PageLogsChooser> chooserDefinition = new ChooserDefinition();
        chooserDefinition.setLabel("Page Logs");
        chooserDefinition.setImplementationClass(PageLogsChooser.class);

        chooserDefinition.getActions().remove("choose");
        chooserDefinition.getActions().remove("commit");
        chooserDefinition.getActions().remove("cancel");
        chooserDefinition.getActions().put("close", new CloseActionDefinition());

        DefaultEditorActionLayoutDefinition footerLayout = new DefaultEditorActionLayoutDefinition();
        footerLayout.setPrimaryActions(Arrays.asList("close"));
        chooserDefinition.setFooterLayout(footerLayout);

        chooserController.openChooser(chooserDefinition);

        try {
           // initMailSlurp();
            //initMailSendGrid();
        } catch (Exception e) {
            System.out.println("Error occurs during send email.");
        }
    }

    public void initMailSlurp() throws Exception {
        // get API KEY for mailslurp from environment variable
        if (StringUtils.isBlank(apiKey)) {
            throw new Exception("Must provide API KEY");
        }

        // IMPORTANT set timeout for the http client
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT, TimeUnit.MILLISECONDS)
                .writeTimeout(TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(TIMEOUT, TimeUnit.MILLISECONDS)
                .build();

        apiClient = Configuration.getDefaultApiClient();

        // IMPORTANT set api client timeouts
        apiClient.setConnectTimeout(TIMEOUT.intValue());
        apiClient.setWriteTimeout(TIMEOUT.intValue());
        apiClient.setReadTimeout(TIMEOUT.intValue());

        // IMPORTANT set API KEY and client
        apiClient.setHttpClient(httpClient);
        apiClient.setApiKey(apiKey);


        InboxControllerApi inboxControllerApi = new InboxControllerApi(apiClient);
        Inbox inbox = inboxControllerApi.createInbox(null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null);

        // simple sending
        SendEmailOptions sendEmailOptions = new SendEmailOptions()
                .to(Lists.newArrayList("trung.luu@magnolia-cms.com"))
                .subject("Test")
                .body("Hello");
        inboxControllerApi.sendEmail(inbox.getId(), sendEmailOptions);
    }

    public void initMailSendGrid() throws Exception {
        Email from = new Email("luuhoangtrung@gmail.com");
        String subject = "Sending with Twilio SendGrid is Fun";
        Email to = new Email("trung.luu@magnolia-cms.com");
        Content content = new Content("text/plain", "and easy to do anywhere, even with Java");
        Mail mail = new Mail(from, subject, to, content);

        SendGrid sg = new SendGrid("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");
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sg.api(request);
            System.out.println(response.getStatusCode());
            System.out.println(response.getBody());
            System.out.println(response.getHeaders());
        } catch (IOException ex) {
            throw ex;
        }
    }

}
