/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.pages.history.action;

import info.magnolia.commands.CommandsManager;
import info.magnolia.context.Context;
import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.NodeNameHelper;
import info.magnolia.pages.history.PagesHistoryModule;
import info.magnolia.pages.history.util.CommonUtil;
import info.magnolia.ui.ValueContext;
import info.magnolia.ui.contentapp.action.MarkAsDeletedCommandAction;
import info.magnolia.ui.contentapp.async.AsyncActionExecutor;
import info.magnolia.ui.datasource.jcr.JcrDatasource;

import java.util.Map;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Log MarkAsDeletedCommandAction.
 */
public class LogMarkAsDeletedCommandAction extends MarkAsDeletedCommandAction {

    private static final Logger log = LoggerFactory.getLogger(LogMarkAsDeletedCommandAction.class);

    private final NodeNameHelper nodeNameHelper;
    private final ValueContext<Node> valueContext;
    private final PagesHistoryModule pagesHistoryModule;

    @Inject
    public LogMarkAsDeletedCommandAction(LogMarkAsDeletedCommandActionDefinition definition,
                                         CommandsManager commandsManager,
                                         ValueContext<Node> valueContext,
                                         Context context,
                                         AsyncActionExecutor asyncActionExecutor,
                                         JcrDatasource datasource,
                                         PagesHistoryModule pagesHistoryModule,
                                         NodeNameHelper nodeNameHelper) {
        super(definition, commandsManager, valueContext, context, asyncActionExecutor, datasource);
        this.nodeNameHelper = nodeNameHelper;
        this.valueContext = valueContext;
        this.pagesHistoryModule = pagesHistoryModule;
    }

    @Override
    protected Map<String, Object> buildParams(Node item) {
        try {
            Node current = valueContext.getSingleOrThrow();
            pagesHistoryModule.setLoginUserName(MgnlContext.getUser().getName());
            pagesHistoryModule.setDeletedElement(pagesHistoryModule.getMgnlTemplate(current));
            pagesHistoryModule.setDeletedPagePath(current.getPath());
            pagesHistoryModule.setDeletedType("PAGE");

            // Store tasks history
            CommonUtil.storeTasksHistory(nodeNameHelper,
                    current,
                    "DELETE",
                    MgnlContext.getUser().getName());
        } catch (RepositoryException e) {
            log.warn("An error occurs during store page logs info of delete action.",  e.getMessage());
        }
        return super.buildParams(item);
    }
}
