/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.pages.history;

import static info.magnolia.context.MgnlContext.*;

import info.magnolia.cms.security.JCRSessionOp;
import info.magnolia.cms.security.SilentSessionOp;
import info.magnolia.jcr.RuntimeRepositoryException;
import info.magnolia.jcr.util.NodeNameHelper;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.ModuleLifecycle;
import info.magnolia.module.ModuleLifecycleContext;
import info.magnolia.observation.WorkspaceEventListenerRegistration;
import info.magnolia.registry.RegistrationException;
import info.magnolia.rendering.template.TemplateDefinition;
import info.magnolia.rendering.template.registry.TemplateDefinitionRegistry;
import info.magnolia.repository.RepositoryConstants;

import java.util.Calendar;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.observation.Event;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is optional and represents the configuration for the magnolia-pages-history module.
 * By exposing simple getter/setter/adder methods, this bean can be configured via content2bean
 * using the properties and node from <tt>config:/modules/magnolia-pages-history</tt>.
 * If you don't need this, simply remove the reference to this class in the module descriptor xml.
 * See https://documentation.magnolia-cms.com/display/DOCS/Module+configuration for information about module configuration.
 */
public class PagesHistoryModule implements ModuleLifecycle {

    private static final Logger log = LoggerFactory.getLogger(PagesHistoryModule.class);

    private WorkspaceEventListenerRegistration.Handle websiteWorkspaceObserver;

    @Inject
    private TemplateDefinitionRegistry templateDefinitionRegistry;
    @Inject
    private NodeNameHelper nodeNameHelper;
    private String loginUserName;
    private String deletedElement;
    private String deletedPagePath;
    private String deletedType;

    @Override
    public void start(ModuleLifecycleContext moduleLifecycleContext) {
        try {
            websiteWorkspaceObserver = WorkspaceEventListenerRegistration.observe(RepositoryConstants.WEBSITE, "/", events -> {
                while (events.hasNext()) {
                    Event event = events.nextEvent();
                    try {
                        String path = event.getPath();
                        String[] pathArr = path.split("/");
                        String name = pathArr[pathArr.length - 1];
                        if (event.getType() == Event.NODE_REMOVED
                                && !isTmpDeletedNode(path)
                                && !isTmpOrdrerStorage(path)
                                && !isVersionStorage(path)) {
                            processForNodeRemoved(path);
                        } else if (!isMetaContent(name) && !isVersionStorage(path)) {
                            processForAllNodesAndProperties(event, name, path);
                        }
                    } catch (RepositoryException e) {
                        throw new RuntimeRepositoryException(e);
                    }
                }
            }).withSubNodes(true).withDelay(2000L, 2000L).register();
        } catch (RepositoryException e) {
            throw new RuntimeRepositoryException(e);
        }
    }

    private boolean isTmpOrdrerStorage(String path) {
        return path.contains("TMP_ORDER_STORAGE");
    }

    private boolean isTmpDeletedNode(String path) {
        return path.contains("mgnl:tmpDeleted");
    }

    private void processForAllNodesAndProperties(Event event, String name, String path) {
        doInSystemContext(new SilentSessionOp<Node>("history") {
            @Override
            public Node doExec(Session session) {
                try {
                    Node currentNode = getInstance()
                            .getJCRSession(RepositoryConstants.WEBSITE)
                            .getNodeByIdentifier(event.getIdentifier());
                    Node currentPage = getPage(currentNode);
                    Node pageLogs = initPageLogs(session, currentPage.getPath(), path);
                    pageLogs.setProperty("mgnlTemplate", getMgnlTemplate(currentNode));
                    pageLogs.setProperty("pageElement", getPageElement(currentPage.getSession(),
                            path.replaceFirst("/", "")));
                    pageLogs.setProperty("pageCreatedBy", PropertyUtil.getString(currentPage, "mgnl:createdBy", ""));
                    pageLogs.setProperty("pageCreatedDate", PropertyUtil.getDate(currentPage, "mgnl:created"));
                    pageLogs.setProperty("pageLastModifiedBy", NodeTypes.LastModified.getLastModifiedBy(currentPage));
                    pageLogs.setProperty("pageLastModifiedDate", NodeTypes.LastModified.getLastModified(currentPage));

                    // Switch
                    switch (event.getType()) {
                    case Event.NODE_ADDED:
                        pageLogs.setProperty("eventType", "ADDED");
                        break;
                    case Event.NODE_MOVED:
                        pageLogs.setProperty("eventType", "MOVED");
                        break;
                    case Event.PROPERTY_ADDED:
                        pageLogs.setProperty("pageProperty", name);
                        pageLogs.setProperty("eventType", "ADDED");
                        break;
                    case Event.PROPERTY_CHANGED:
                        pageLogs.setProperty("pageProperty", name);
                        pageLogs.setProperty("eventType", "CHANGED");
                        break;
                    case Event.PROPERTY_REMOVED:
                        pageLogs.setProperty("pageProperty", name);
                        pageLogs.setProperty("eventType", "REMOVED");
                        break;
                    default:
                        break;
                    }
                    // Save
                    session.save();
                } catch (RepositoryException e) {
                    log.warn("An error occurs during check pages history all nodes and properties.", e.getMessage());
                }
                return null;
            }
        }, true);
    }

    private void processForNodeRemoved(String path) {
        try {
            doInSystemContext(new JCRSessionOp<Node>("history") {
                @Override
                public Node exec(Session session) throws RepositoryException {
                    String deletedPagePath = getDeletedPagePath();
                    if (StringUtils.isNotEmpty(deletedPagePath)) {
                        Node pageLogs = initPageLogs(session, deletedPagePath, path);
                        pageLogs.setProperty("eventType", "REMOVED");
                        pageLogs.setProperty("mgnlTemplate", getDeletedElement());
                        pageLogs.setProperty("pageElement", getDeletedType());
                        pageLogs.setProperty("pageLastModifiedBy", getLoginUserName());
                        pageLogs.setProperty("pageLastModifiedDate", Calendar.getInstance());
                        // Save
                        session.save();
                    }
                    return null;
                }
            });
        } catch (RepositoryException e) {
            log.warn("An error occurs during check pages history removed nodes.", e.getMessage());
        }
    }

    private boolean isVersionStorage(String path) {
        return path.contains("jcr:system");
    }

    private Node initPageLogs(Session session, String logPath, String eventPath) throws RepositoryException {
        Node pageHistory;
        Node root = session.getRootNode();
        String pageLogPath = logPath.replaceFirst("/", "");

        if (root.hasNode(pageLogPath)) {
            pageHistory = root.getNode(pageLogPath);
        } else {
            pageHistory = NodeUtil.createPath(root, pageLogPath, NodeTypes.Content.NAME);
        }

        Node pageLogs = pageHistory.addNode(nodeNameHelper.getValidatedName(nodeNameHelper.getUniqueName(pageHistory, "log")), NodeTypes.Content.NAME);
        pageLogs.setProperty("pagePath", eventPath);
        return pageLogs;
    }

    private boolean hasPath(Session session, String eventPath) {
        try {
            return session.getRootNode().hasNode(eventPath);
        } catch (Exception e) {
            return false;
        }
    }

    private boolean isMetaContent(String name) {
        if (NodeTypes.Renderable.TEMPLATE.equals(name)) {
            return false;
        }
        return name.contains("jcr:") || name.contains("mgnl:");
    }

    public String getMgnlTemplate(Node currentNode) {
        String templateName = "";
        try {
            templateName = NodeTypes.Renderable.getTemplate(currentNode);
            final TemplateDefinition template = templateDefinitionRegistry.getTemplateDefinition(templateName);
            if (template != null) {
                templateName = StringUtils.isEmpty(template.getTitle()) ? template.getName() : template.getTitle();
            }
        } catch (RegistrationException e) {
            log.warn("Could not find template definition with name [{}].", templateName);
            return templateName;
        } catch (RepositoryException re) {
            log.warn("Could not access the template property for node {}.", currentNode, re);
            return templateName;
        }
        return templateName;
    }

    public String getPageElement(Session session, String eventPath) throws RepositoryException {
        String pageElement = "";
        if (hasPath(session, eventPath)) {
            return session.getRootNode().getNode(eventPath).getPrimaryNodeType().getName().replace("mgnl:", "").toUpperCase();
        } else {
            pageElement = "PROPERTY";
        }
        return pageElement;
    }

    public Node getPage(Node current) throws RepositoryException {
        return NodeTypes.Page.NAME.equals(current.getPrimaryNodeType().getName()) ? current: NodeUtil.getNearestAncestorOfType(current, NodeTypes.Page.NAME);
    }

    @Override
    public void stop(ModuleLifecycleContext moduleLifecycleContext) {
        if (websiteWorkspaceObserver != null) {
            try {
                websiteWorkspaceObserver.unregister();
            } catch (RepositoryException e) {
                log.error("Unable to remove event listener from workspace {}", RepositoryConstants.WEBSITE, e);
            }
        }
    }

    public String getLoginUserName() {
        return loginUserName;
    }

    public void setLoginUserName(String loginUserName) {
        this.loginUserName = loginUserName;
    }

    public String getDeletedElement() {
        return deletedElement;
    }

    public void setDeletedElement(String deletedElement) {
        this.deletedElement = deletedElement;
    }

    public String getDeletedPagePath() {
        return deletedPagePath;
    }

    public void setDeletedPagePath(String deletedPagePath) {
        this.deletedPagePath = deletedPagePath;
    }

    public String getDeletedType() {
        return deletedType;
    }

    public void setDeletedType(String deletedType) {
        this.deletedType = deletedType;
    }
}
