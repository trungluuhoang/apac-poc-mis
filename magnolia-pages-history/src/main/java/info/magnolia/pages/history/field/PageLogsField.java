/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.pages.history.field;

import info.magnolia.context.Context;
import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.RuntimeRepositoryException;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.ui.ValueContext;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.inject.Provider;
import javax.jcr.Item;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.components.grid.HeaderCell;
import com.vaadin.ui.components.grid.HeaderRow;
import com.vaadin.ui.renderers.ComponentRenderer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * PageLogsField uses to render logs info by grid.
 */
public class PageLogsField extends CustomField<String> {

    private static final Logger log = LoggerFactory.getLogger(PageLogsField.class);

    private final ValueContext<Item> valueContext;

    private Provider<Context> contextProvider;
    private Grid<PageLog> grid;
    private Node node;

    public PageLogsField(Provider<Context> contextProvider, ValueContext<Item> valueContext) {
        this.contextProvider = contextProvider;
        this.valueContext = valueContext;

        this.grid = new Grid();
        this.grid.setSelectionMode(Grid.SelectionMode.NONE);
    }

    @Override
    protected void doSetValue(String value) {
        // do nothing
    }

    @Override
    public String getValue() {
        return "";
    }

    @Override
    protected Component initContent() {
        VerticalLayout layout = new VerticalLayout();
        layout.addStyleName("page-logs-info");
        layout.setMargin(false);
        layout.addComponent(grid);

        Optional value = valueContext.getSingle();
        if (value.isPresent() && value.get() instanceof Node) {
            node = (Node) value.get();
        } else {
            log.warn("Value context is not present or isn't a Node. Field will not be initialized.");
            return null;
        }

        List<Node> pageLogs = getPageLogs(node);
        try {
            addPageLogsToGrid(pageLogs, node.getPath());
        } catch (RepositoryException e) {
            throw new RuntimeRepositoryException(e);
        }
        return layout;
    }

    private void addPageLogsToGrid(List<Node> pageLogs, String selectedPagePath) throws RepositoryException {
        grid.setWidth(100, Unit.PERCENTAGE);
        grid.setSelectionMode(Grid.SelectionMode.NONE);

        // do the setup only if we have logs
        if (CollectionUtils.isNotEmpty(pageLogs)) {

            Grid.Column pagePath = grid.addColumn(pageLog -> new Label(pageLog.getPagePath(), ContentMode.HTML), new ComponentRenderer())
                    .setId("pagePath")
                    .setCaption("Path")
                    .setExpandRatio(90)
                    .setSortable(false)
                    .setHandleWidgetEvents(true);

            Grid.Column mgnlTemplate = grid.addColumn(pageLog -> new Label(pageLog.getMgnlTemplate(), ContentMode.HTML), new ComponentRenderer())
                    .setId("mgnlTemplate")
                    .setCaption("Component")
                    .setExpandRatio(10)
                    .setSortable(false)
                    .setHandleWidgetEvents(true);

            Grid.Column eventType = grid.addColumn(pageLog -> new Label(pageLog.getEventType(), ContentMode.HTML), new ComponentRenderer())
                    .setId("eventType")
                    .setCaption("Event Type")
                    .setExpandRatio(10)
                    .setSortable(false)
                    .setHandleWidgetEvents(true);

            // Hide this column for now
            //Grid.Column pageElement = grid.addColumn(pageLog -> new Label(pageLog.getPageElement(), ContentMode.HTML), new ComponentRenderer())
            //        .setId("pageElement")
            //        .setCaption("Element")
            //        .setExpandRatio(10)
            //        .setHandleWidgetEvents(true);

            Grid.Column pageProperty = grid.addColumn(pageLog -> new Label(pageLog.getPageProperty(), ContentMode.HTML), new ComponentRenderer())
                    .setId("pageProperty")
                    .setCaption("Property")
                    .setExpandRatio(10)
                    .setSortable(false)
                    .setHandleWidgetEvents(true);

            Grid.Column pageLastModifiedBy = grid.addColumn(pageLog -> new Label(pageLog.getPageLastModifiedBy(), ContentMode.HTML), new ComponentRenderer())
                    .setId("pageLastModifiedBy")
                    .setCaption("Last Modified By")
                    .setExpandRatio(10)
                    .setSortable(false)
                    .setHandleWidgetEvents(true);

            grid.addColumn(pageLog -> {
                SimpleDateFormat dateFormat = createDateFormat(contextProvider);
                String label = pageLog.getPageLastModifiedDate() == null ? ""
                        : dateFormat.format(pageLog.getPageLastModifiedDate());
                return new Label(label, ContentMode.HTML);
            }, new ComponentRenderer())
                    .setId("pageLastModifiedDate")
                    .setCaption("Last Modified Date")
                    .setExpandRatio(20)
                    .setSortable(false)
                    .setHandleWidgetEvents(true);

            AtomicInteger idx = new AtomicInteger();
            List<PageLog> pages = pageLogs.stream()
                    .sorted(Comparator.comparingLong(this::jcrLastModifiedDatePropertyInMillis).reversed())
                    .map(pageLog -> {
                        idx.getAndIncrement();
                        return createPageLogs(String.valueOf(idx.get()), pageLog);
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());

            // Remove filter for now
            //addFiltersToHeader(pagePath, mgnlTemplate, eventType, pageProperty, pageLastModifiedBy, pages);
            grid.setItems(pages);
        } else {
            PageLog noLogs = new PageLog("0", selectedPagePath, "", "No Logs", "", "", "", null);
            grid.setItems(noLogs);
        }
    }

    private void addFiltersToHeader(Grid.Column pagePath,
                                    Grid.Column mgnlTemplate,
                                    Grid.Column eventType,
                                    Grid.Column pageProperty,
                                    Grid.Column pageLastModifiedBy,
                                    List<PageLog> pages) {
        HeaderRow filterRow = grid.appendHeaderRow();
        createTextFilter(filterRow, pagePath, pages, "pagePath");
        createTextFilter(filterRow, pagePath, pages, "mgnlTemplate");
        //createTextFilter(filterRow, pageElement, pages, "pageElement");
        createTextFilter(filterRow, pageProperty, pages, "pageProperty");
        createTextFilter(filterRow, pageLastModifiedBy, pages, "pageLastModifiedBy");

        HeaderCell eventTypeCell = filterRow.getCell(eventType);
        ComboBox<String> comboBox = new ComboBox();
        comboBox.setPlaceholder("Filter...");
        comboBox.setItems("All", "ADDED", "MOVED", "CHANGED", "REMOVED",
                "REQUEST_TO_PUBLISH",
                "REQUEST_TO_PUBLISH_DELETION",
                "REQUEST_TO_PUBLISH_RECURSIVE",
                "REQUEST_TO_UNPUBLISH");
        comboBox.setValue("All");
        comboBox.addValueChangeListener(l -> {
            String value = l.getValue();
            if (StringUtils.equals(value, "All")) {
                grid.setItems(pages);
            } else {
                Set<PageLog> filteredPages = pages.stream()
                        .filter(p -> value.equalsIgnoreCase(p.eventType))
                        .collect(Collectors.toSet());
                grid.setItems(filteredPages);
            }
        });
        eventTypeCell.setComponent(comboBox);
    }

    private void createTextFilter(HeaderRow filterRow, Grid.Column column, List<PageLog> pages, String type) {
        HeaderCell cell = filterRow.getCell(column);
        TextField textField = new TextField();
        textField.setPlaceholder("Filter...");
        textField.addValueChangeListener(l -> {
            Set<PageLog> filteredPages = pages.stream()
                    .filter(p -> {
                        if ("pagePath".equalsIgnoreCase(type)) {
                            return StringUtils.containsIgnoreCase(p.pagePath, l.getValue());
                        } else if ("pageProperty".equalsIgnoreCase(type)) {
                            return StringUtils.containsIgnoreCase(p.pageProperty, l.getValue());
                        } else if ("pageLastModifiedBy".equalsIgnoreCase(type)) {
                            return StringUtils.containsIgnoreCase(p.pageLastModifiedBy, l.getValue());
                        }
                        return false;
                    })
                    .collect(Collectors.toSet());
            grid.setItems(filteredPages);
        });
        cell.setComponent(textField);
    }


    private PageLog createPageLogs(String number, Node pageLog) {
        PageLog logs = new PageLog();
        logs.setNumber(number);
        logs.setPagePath(PropertyUtil.getString(pageLog, "pagePath", ""));
        logs.setMgnlTemplate(PropertyUtil.getString(pageLog, "mgnlTemplate", ""));
        logs.setEventType(PropertyUtil.getString(pageLog, "eventType", ""));
        logs.setPageElement(PropertyUtil.getString(pageLog, "pageElement", ""));
        logs.setPageProperty(PropertyUtil.getString(pageLog, "pageProperty", ""));
        logs.setPageLastModifiedBy(PropertyUtil.getString(pageLog, "pageLastModifiedBy", ""));
        try {
            if (pageLog.hasProperty("pageLastModifiedDate")) {
                logs.setPageLastModifiedDate(PropertyUtil.getDate(pageLog, "pageLastModifiedDate").getTime());
            }
        } catch (RepositoryException e) {
            log.warn("An error occurs during get last modified date.");
        }
        return logs;
    }

    private List<Node> getPageLogs(Node page) {
        try {
            String path = page.getPath().replaceFirst("/", "");
            Node historyRoot = MgnlContext.getJCRSession("history").getRootNode();
            if (historyRoot.hasNode(path)) {
                Node pageLog = historyRoot.getNode(path);
                Iterable<Node> logsIterable = NodeUtil.getNodes(pageLog, NodeTypes.Content.NAME);
                if (logsIterable != null) {
                    return Lists.newArrayList(logsIterable).stream().filter(PageLogsField::hasLogName).collect(Collectors.toList());
                }
            }
        } catch (RepositoryException e) {
            log.warn("Error getting page log {} {}", node, e);
        }
        return Collections.emptyList();
    }

    private static boolean hasLogName(Node pageLog) {
        try {
            return pageLog.getName().contains("log");
        } catch (RepositoryException e) {
            log.warn("An error occurs during check log name.");
        }
        return false;
    }

    private long jcrLastModifiedDatePropertyInMillis(Node node) {
        long defaultMillis = 0L;
        try {
            if (node.hasProperty("pageLastModifiedDate")) {
                return PropertyUtil.getDate(node, "pageLastModifiedDate").getTimeInMillis();
            }
        } catch (RepositoryException e) {
            log.warn("An error occurs during get last modified date.", e.getMessage());
            return defaultMillis;
        }
        return defaultMillis;
    }

    /**
     * A page represents a row in the grid.
     */
    @Data
    @EqualsAndHashCode(of = { "number", "pagePath", "mgnlTemplate", "eventType", "pageElement", "pageProperty", "pageLastModifiedBy", "pageLastModifiedDate" })
    @AllArgsConstructor
    @NoArgsConstructor
    static class PageLog {
        private String number;
        private String pagePath;
        private String mgnlTemplate;
        private String eventType;
        private String pageElement;
        private String pageProperty;
        private String pageLastModifiedBy;
        private Date pageLastModifiedDate;
    }

    static SimpleDateFormat createDateFormat(Provider<Context> context) {
        SimpleDateFormat df = new SimpleDateFormat("MMM dd, yyyy hh:mm aa", getCurrentLocale(context));
        df.setTimeZone(getCurrentZoneId(context));
        return df;
    }

    static Locale getCurrentLocale(Provider<Context> context) {
        return (Locale) Optional.ofNullable(((Context) context.get()).getUser().getProperty("language")).filter(StringUtils::isNotBlank).map((lang) -> {
            return StringUtils.substringBefore(lang, "_");
        }).map(Locale::new).orElse(Locale.getDefault());
    }

    static TimeZone getCurrentZoneId(Provider<Context> context) {
        return (TimeZone) Optional.ofNullable(((Context) context.get()).getUser().getProperty("timezone")).filter(StringUtils::isNotBlank).map(TimeZone::getTimeZone).orElse(TimeZone.getDefault());
    }

}
