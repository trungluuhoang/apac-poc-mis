/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.pages.history.action;

import info.magnolia.commands.CommandsManager;
import info.magnolia.context.Context;
import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.NodeNameHelper;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.module.workflow.action.WorkflowCommandAction;
import info.magnolia.pages.history.util.CommonUtil;
import info.magnolia.ui.CloseHandler;
import info.magnolia.ui.ValueContext;
import info.magnolia.ui.contentapp.async.AsyncActionExecutor;
import info.magnolia.ui.datasource.jcr.JcrDatasource;
import info.magnolia.ui.editor.FormView;

import java.util.Calendar;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Log the workflow actions.
 */
public class LogWorkflowCommandAction extends WorkflowCommandAction {

    private static final Logger log = LoggerFactory.getLogger(LogWorkflowCommandAction.class);
    private LogWorkflowCommandActionDefinition definition;
    private ValueContext<Node> valueContext;
    private NodeNameHelper nodeNameHelper;

    @Inject
    public LogWorkflowCommandAction(LogWorkflowCommandActionDefinition definition,
                                    CommandsManager commandsManager,
                                    ValueContext<Node> valueContext,
                                    Context context,
                                    AsyncActionExecutor asyncActionExecutor,
                                    FormView<Node> form,
                                    CloseHandler closeHandler,
                                    JcrDatasource datasource,
                                    NodeNameHelper nodeNameHelper) {
        super(definition, commandsManager, valueContext, context, asyncActionExecutor, form, closeHandler, datasource);
        this.definition = definition;
        this.valueContext = valueContext;
        this.nodeNameHelper = nodeNameHelper;
    }

    @Override
    public void execute() {
        Node current = valueContext.getSingleOrThrow();
        storePageLogs(current);
        super.execute();
    }

    private void storePageLogs(Node current) {
        try {
            Node pageHistory;
            Session session = MgnlContext.getJCRSession("history");
            Node root = session.getRootNode();
            String pagePath = current.getPath().replaceFirst("/", "");
            if (root.hasNode(pagePath)) {
                pageHistory = root.getNode(pagePath);
            } else {
                pageHistory = NodeUtil.createPath(root, pagePath, NodeTypes.Content.NAME);
            }
            Node pageLogs = pageHistory.addNode(nodeNameHelper.getValidatedName(nodeNameHelper.getUniqueName(pageHistory, "log")), NodeTypes.Content.NAME);
            pageLogs.setProperty("pageElement", "PAGE");
            pageLogs.setProperty("pagePath", current.getPath());
            pageLogs.setProperty("eventType", definition.getPublishType());
            pageLogs.setProperty("pageLastModifiedBy", MgnlContext.getUser().getName());
            pageLogs.setProperty("pageLastModifiedDate", Calendar.getInstance());
            session.save();

            // Store tasks history
            CommonUtil.storeTasksHistory(nodeNameHelper,
                    current,
                    definition.getPublishType(),
                    MgnlContext.getUser().getName());
        } catch (RepositoryException e) {
            log.warn("An error occurs during store page logs when submitting.", e.getMessage());
        }
    }
}
