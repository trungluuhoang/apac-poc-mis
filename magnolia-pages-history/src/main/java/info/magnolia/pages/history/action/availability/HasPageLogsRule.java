/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.pages.history.action.availability;

import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.ui.api.availability.AvailabilityDefinition;
import info.magnolia.ui.api.availability.AvailabilityRuleDefinition;
import info.magnolia.ui.availability.rule.AbstractAvailabilityRule;

import javax.inject.Inject;
import javax.jcr.Item;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Rule for checking has page logs or not.
 */
public class HasPageLogsRule extends AbstractAvailabilityRule<Item, AvailabilityRuleDefinition> {

    private static final Logger log = LoggerFactory.getLogger(HasPageLogsRule.class);

    @Inject
    HasPageLogsRule(AvailabilityDefinition availabilityDefinition, AvailabilityRuleDefinition ruleDefinition) {
        super(availabilityDefinition, ruleDefinition);
    }

    @Override
    protected boolean isAvailableFor(Item item) {
        if (item != null && item.isNode()) {
            Node node = (Node) item;
            try {
                String path = node.getPath().replaceFirst("/", "");
                Node root = MgnlContext.getJCRSession("history").getRootNode();
                if (root.hasNode(path)) {
                    Iterable<Node> logsIterable = NodeUtil.getNodes(root.getNode(path), NodeTypes.Content.NAME);
                    if (logsIterable != null) {
                        for (Node pageLog: logsIterable) {
                            if (pageLog.getName().contains("log")) {
                                return true;
                            }
                        }
                    }
                }
            } catch (RepositoryException e) {
                log.warn("Error evaluating is page logs rule for node [{}], returning false: {}", NodeUtil.getPathIfPossible(node), e.getMessage());
                return false;
            }
        }
        return false;
    }
}
