/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.pages.history.action;

import info.magnolia.cms.security.SecuritySupport;
import info.magnolia.context.Context;
import info.magnolia.i18nsystem.SimpleTranslator;
import info.magnolia.jcr.util.NodeNameHelper;
import info.magnolia.pages.history.util.CommonUtil;
import info.magnolia.task.Task;
import info.magnolia.task.TasksManager;
import info.magnolia.task.app.actions.ClaimTasksAction;
import info.magnolia.task.app.actions.ClaimTasksActionDefinition;
import info.magnolia.ui.ValueContext;

import info.magnolia.ui.framework.message.MessagesManager;

import javax.inject.Inject;
import javax.jcr.Node;

/**
 * Log based on ClaimTasksAction.
 */
public class LogClaimTasksAction extends ClaimTasksAction {

    private final ValueContext<Task> valueContext;
    private final NodeNameHelper nodeNameHelper;
    private final Context context;

    @Inject
    public LogClaimTasksAction(ClaimTasksActionDefinition definition,
                               TasksManager taskManager,
                               Context context,
                               MessagesManager messagesManager,
                               SimpleTranslator i18n,
                               SecuritySupport securitySupport,
                               ValueContext<Task> valueContext,
                               NodeNameHelper nodeNameHelper) {
        super(definition, taskManager, context, messagesManager, i18n, securitySupport, valueContext);
        this.context = context;
        this.valueContext = valueContext;
        this.nodeNameHelper = nodeNameHelper;
    }

    @Override
    public void execute() {
        String userId = this.context.getUser().getName();
        Task task = valueContext.getSingleOrThrow();
        Node content = CommonUtil.getSelectedFromTaskContent(task.getContent());
        if (content != null) {
            CommonUtil.storeTasksHistory(nodeNameHelper,
                    content,
                    "ASSIGN TO ME",
                    userId);
        }
        super.execute();
    }

}
