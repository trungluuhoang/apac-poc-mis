/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.pages.history.action;

import info.magnolia.module.workflow.action.WorkflowCommandActionDefinition;

/**
 * Definition for LogWorkflowCommandAction.
 */
public class LogWorkflowCommandActionDefinition extends WorkflowCommandActionDefinition {

    private String publishType;

    public LogWorkflowCommandActionDefinition() {
        this.setImplementationClass(LogWorkflowCommandAction.class);
        this.setCatalog("workflow");
        this.setCommand("activate");
    }

    public String getPublishType() {
        return publishType;
    }

    public void setPublishType(String publishType) {
        this.publishType = publishType;
    }
}
