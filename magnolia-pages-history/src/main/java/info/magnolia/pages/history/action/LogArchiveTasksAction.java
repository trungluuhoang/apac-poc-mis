/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.pages.history.action;

import info.magnolia.context.Context;
import info.magnolia.jcr.util.NodeNameHelper;
import info.magnolia.pages.history.util.CommonUtil;
import info.magnolia.task.Task;
import info.magnolia.task.TasksManager;
import info.magnolia.task.app.actions.ArchiveTasksAction;
import info.magnolia.task.app.actions.ArchiveTasksActionDefinition;
import info.magnolia.ui.ValueContext;
import info.magnolia.ui.api.app.SubAppContext;

import javax.inject.Inject;
import javax.jcr.Node;

/**
 * Log based on ArchiveTasksAction.
 */
public class LogArchiveTasksAction extends ArchiveTasksAction {

    private final Context context;
    private final NodeNameHelper nodeNameHelper;
    private final ValueContext<Task> valueContext;
    private final SubAppContext subAppContext;

    @Inject
    public LogArchiveTasksAction(ArchiveTasksActionDefinition definition,
                                 TasksManager taskManager,
                                 ValueContext<Task> valueContext,
                                 Context context,
                                 NodeNameHelper nodeNameHelper,
                                 SubAppContext subAppContext) {
        super(definition, taskManager, valueContext);
        this.context = context;
        this.nodeNameHelper = nodeNameHelper;
        this.valueContext = valueContext;
        this.subAppContext = subAppContext;
    }

    @Override
    public void execute() {
        String userId = this.context.getUser().getName();
        Task task = valueContext.getSingleOrThrow();
        Node content = CommonUtil.getSelectedFromTaskContent(task.getContent());
        if (content != null) {
            CommonUtil.storeTasksHistory(nodeNameHelper,
                    content,
                    "ARCHIVE",
                    userId);
        }
        super.execute();
        // Show Notification
        subAppContext.getAppContext().sendLocalMessage(CommonUtil.createMessage(task.getContent(), "archived"));
    }
}
