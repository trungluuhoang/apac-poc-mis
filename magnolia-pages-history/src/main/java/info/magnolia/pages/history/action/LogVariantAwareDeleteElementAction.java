/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.pages.history.action;

import info.magnolia.context.MgnlContext;
import info.magnolia.pages.app.detail.action.VariantAwareDeleteElementAction;
import info.magnolia.pages.app.detail.PageEditorStatus;
import info.magnolia.pages.history.PagesHistoryModule;
import info.magnolia.personalization.variant.EditorVariantResolver;
import info.magnolia.ui.ValueContext;
import info.magnolia.ui.observation.DatasourceObservation;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Log delete action in pages detail mode.
 */
public class LogVariantAwareDeleteElementAction extends VariantAwareDeleteElementAction {

    private static final Logger log = LoggerFactory.getLogger(LogVariantAwareDeleteElementAction.class);

    private final ValueContext<Node> valueContext;
    private final PagesHistoryModule pagesHistoryModule;

    @Inject
    public LogVariantAwareDeleteElementAction(LogVariantAwareDeleteElementActionDefinition definition,
                                              ValueContext<Node> valueContext,
                                              DatasourceObservation.Manual datasourceObservation,
                                              PageEditorStatus pageEditorStatus,
                                              EditorVariantResolver editorVariantResolver,
                                              PagesHistoryModule pagesHistoryModule) {
        super(definition, valueContext, datasourceObservation, pageEditorStatus, editorVariantResolver);
        this.valueContext = valueContext;
        this.pagesHistoryModule = pagesHistoryModule;
    }

    @Override
    public void execute() {
        try {
            Node current = valueContext.getSingleOrThrow();
            pagesHistoryModule.setLoginUserName(MgnlContext.getUser().getName());
            pagesHistoryModule.setDeletedElement(pagesHistoryModule.getMgnlTemplate(current));
            pagesHistoryModule.setDeletedPagePath(pagesHistoryModule.getPage(current).getPath());
            pagesHistoryModule.setDeletedType(pagesHistoryModule.getPageElement(current.getSession(), current.getPath()));
        } catch (RepositoryException e) {
            log.warn("An error occurs during store page logs info of delete element action.", e.getMessage());
        }
        super.execute();
    }
}
