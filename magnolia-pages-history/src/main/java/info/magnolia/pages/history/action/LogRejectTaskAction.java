/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.pages.history.action;

import info.magnolia.context.Context;
import info.magnolia.jcr.util.NodeNameHelper;
import info.magnolia.pages.history.util.CommonUtil;
import info.magnolia.task.Task;
import info.magnolia.task.TasksManager;
import info.magnolia.ui.admincentral.shellapp.pulse.task.DefaultTaskDetailPresenter;
import info.magnolia.ui.admincentral.shellapp.pulse.task.action.RejectTaskAction;
import info.magnolia.ui.admincentral.shellapp.pulse.task.action.RejectTaskActionDefinition;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.shell.Shell;
import info.magnolia.ui.dialog.formdialog.FormDialogPresenter;

import javax.inject.Inject;
import javax.jcr.Node;

/**
 * Log based on RejectTaskAction.
 */
public class LogRejectTaskAction extends RejectTaskAction {

    private final Context context;
    private final NodeNameHelper nodeNameHelper;

    @Inject
    public LogRejectTaskAction(RejectTaskActionDefinition definition,
                               Task task,
                               TasksManager taskManager,
                               DefaultTaskDetailPresenter taskPresenter,
                               FormDialogPresenter formDialogPresenter,
                               UiContext uiContext,
                               Shell shell,
                               Context context,
                               NodeNameHelper nodeNameHelper) {
        super(definition, task, taskManager, taskPresenter, formDialogPresenter, uiContext, shell);
        this.context = context;
        this.nodeNameHelper = nodeNameHelper;
    }


    @Override
    protected void executeTask(final TasksManager taskManager, final Task task) {
        final String userId = this.context.getUser().getName();
        Node content = CommonUtil.getSelectedFromTaskContent(task.getContent());
        if (content != null) {
            CommonUtil.storeTasksHistory(nodeNameHelper,
                    content,
                    "REJECT",
                    userId);
        }
        super.executeTask(taskManager, task);
    }

}
