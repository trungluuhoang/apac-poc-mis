/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.pages.history.action;

import info.magnolia.context.Context;
import info.magnolia.pages.history.field.PageLogsField;
import info.magnolia.ui.ValueContext;
import info.magnolia.ui.chooser.Chooser;

import java.util.function.Consumer;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.jcr.Item;

import com.vaadin.ui.Component;

/**
 * Init PageLogsField for rendering grid.
 */
public class PageLogsChooser implements Chooser<String> {

    private PageLogsField item;

    @Inject
    public PageLogsChooser(ValueContext<Item> valueContext, Provider<Context> contextProvider) {
        this.item = new PageLogsField(contextProvider, valueContext);
    }

    @Override
    public Component asVaadinComponent() {
        return item;
    }

    @Override
    public void onChange(Consumer<String> callback) {
        // do nothing
    }

    @Override
    public String getChoice() {
        return item.getValue();
    }

    @Override
    public void setChoice(String choice) {
        // do nothing
    }
}
