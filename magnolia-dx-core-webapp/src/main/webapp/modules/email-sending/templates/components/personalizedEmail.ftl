[#-------------- ASSIGNMENTS --------------]
[#assign title = content.title!]
[#assign api = content.api!]
[#assign key = content.key!]
[#assign receiveEmail = content.receiveEmail!]
[#assign notSendMail = content.notSendMail!]
[#assign username = content.username!]
[#assign serviceId = content.serviceId!]
[#assign templateId = content.templateId!]
[#assign userId = content.userId!]
[#assign accessToken = content.accessToken!]

[#if cmsfn.editMode]
<div>
[#else]
<div data-email-sending="data-email-sending">
[/#if]
	[#if cmsfn.editMode]
	<div class="text-center">
		<p>Sending email component has been added and ready to use.</p>
		<p>This text only display on EditMode</p>
	</div>
	[/#if]
	<form name="emailSending">
		<input type="hidden" name="title" value="${title}"/>
		<textarea style="display:none;" name="body">${cmsfn.decode(content).body!}</textarea>
		<input type="hidden" name="emailReceive" value="${receiveEmail}"/>
		<input type="hidden" name="notSendMail" value="${notSendMail?then('1','0')}"/>
		<input type="hidden" name="username" value="${username}" />
		<input type="hidden" name="serviceId" value="${serviceId}" />
		<input type="hidden" name="templateId" value="${templateId}" />
		<input type="hidden" name="userId" value="${userId}" />
		<input type="hidden" name="accessToken" value="${accessToken}" />
	</form>
</div>

<script defer src="${ctx.contextPath}/.resources/email-sending/webresources/js/email-sending.min.js"></script>