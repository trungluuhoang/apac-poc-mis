#### Setup
* Modify WEB-INF/config/jaas.config
```
magnolia {
  // ensure user is who he claims he is (check pwd for the user)
  // info.magnolia.jaas.sp.jcr.JCRAuthenticationModule requisite;
  info.magnolia.jaas.sp.jcr.JCRAuthenticationModuleExt requisite;
  // retrieve users ACLs
  info.magnolia.jaas.sp.jcr.JCRAuthorizationModule required;
};
```
* Modify [user] dialog definition in Resources app "/magnolia-password-enhancement/decorations/security-app/dialogs/user.yaml"
* Modify [editUserProfile] dialog definition in Resources app "/magnolia-password-enhancement/decorations/ui-admincentral/dialogs/editUserProfile.yaml"
```
form:
  tabs:
    user:
      fields:
        pswd:
          validators:
            isValid:
              class: info.magnolia.ui.form.validator.definition.RegexpValidatorDefinition
              errorMessage: 'Please enter a valid password.'
              pattern: '^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).{8,64}$'
            isKnown:
              class: info.magnolia.ui.editor.validator.PasswordValidatorDefinition
              errorMessage: 'Please enter an un-known password'
          passwordMessage: 'Create a password. It should be at least 8 and max 64 long and contains the following: uppercase letter (A-Z), lowercase letter (a-z), digit (0-9) and special characters.'
```
----------------------------------------------------------------------------
#### Usage
* Enable/Disable magnolia-password-enhancement module by flag: [CONFIG] /modules/magnolia-password-enhancement/config@enable
* [superuser] account will not expire
* Setting:
```
    enable: true        -> enable or disable password expiration checking
    maxPasswordAge: 90  -> Maximum day of the current password from the last changed before expire
    expireAlert: 14     -> Remaining day of the current password to receive the password expiration warning  
    passDictionaries:   -> Pre-defined known password here to for checking validation
      pass1:
        type: Common
        value: 123abc@BC
      pass2:
      ...
```