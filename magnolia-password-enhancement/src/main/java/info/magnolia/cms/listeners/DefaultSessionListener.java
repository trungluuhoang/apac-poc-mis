/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.cms.listeners;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import info.magnolia.cms.managers.DefaultSessionManager;

/**
 * Default session listener.
 */
public class DefaultSessionListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        DefaultSessionManager.instance.sessionCreated(se.getSession());
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        DefaultSessionManager.instance.sessionDestroyed(se.getSession().getId());
    }

}
