/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.cms.filters;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.security.auth.Subject;
import javax.security.auth.login.CredentialException;
import javax.security.auth.login.LoginException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import info.magnolia.audit.AuditLoggingUtil;
import info.magnolia.cms.managers.DefaultSessionManager;
import info.magnolia.cms.security.PrincipalUtil;
import info.magnolia.cms.security.User;
import info.magnolia.cms.security.auth.login.FormLogin;
import info.magnolia.cms.security.auth.login.LoginHandler;
import info.magnolia.cms.security.auth.login.LoginResult;
import info.magnolia.cms.util.RequestDispatchUtil;
import info.magnolia.context.MgnlContext;
import info.magnolia.monitoring.AccessRestrictedException;
import info.magnolia.monitoring.SystemMonitor;
import info.magnolia.objectfactory.Components;

/**
 * This class is used to manage login filter.
 */
public class ManagedLoginFilter extends AbstractMgnlFilter {
    
    private static final Logger log = LoggerFactory.getLogger(ManagedLoginFilter.class);

    @Inject
    DefaultSessionManager sm;
    
    private int maxSessions = 1;
    private boolean kickOutLoggedUser = true;
    
    private Collection<LoginHandler> loginHandlers = new ArrayList<>();

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {

        for (LoginHandler handler : this.getLoginHandlers()) {
            LoginResult loginResult = handler.handle(request, response);
            LoginResult.setCurrentLoginResult(loginResult);

            if (loginResult.getStatus() == LoginResult.STATUS_IN_PROCESS) {
                // special handling to support multi step login mechanisms like ntlm
                // do not continue with the filter chain
                AuditLoggingUtil.log(loginResult, request);
                return;
            } else if (loginResult.getStatus() == LoginResult.STATUS_SUCCEEDED ||
                    loginResult.getStatus() == LoginResult.STATUS_SUCCEEDED_REDIRECT_REQUIRED) {

                if (Components.getComponent(SystemMonitor.class).isMemoryLimitReached()) {
                    final String memoryLimitReachedMessage = String.format(SystemMonitor.MEMORY_LIMIT_IS_REACHED_STRING_FORMAT, "That is why further logins have to be blocked for now.");
                    logSubjectError(memoryLimitReachedMessage, request, new AccessRestrictedException());
                    break;
                }

                Subject subject = loginResult.getSubject();
                if (subject == null) {
                    String errorMsg = "Invalid login result from handler [" + handler.getClass().getName() + "] returned STATUS_SUCCEEDED but no subject";
                    logSubjectError(errorMsg, request, new CredentialException(errorMsg));
                    throw new ServletException("Invalid login result");
                }

                // manage login
                User user = PrincipalUtil.findPrincipal(subject, User.class);
                if (user == null) {
                    throw new IllegalArgumentException("When logging in the Subject must have a info.magnolia.cms.security.User principal.");
                }
                String username = user.getName();
                int loggedins = sm.loggedins(username);
                if (loggedins >= maxSessions) {
                    String errorMsg = "Max sessions [" + maxSessions + "] reached for user [" + username + "]";
                    if (kickOutLoggedUser) {
                        log.error(errorMsg);
                        List<HttpSession> copySessions = sm.getManagedSessions(username).stream().collect(Collectors.toList());
                        for (HttpSession httpSession : copySessions) {
                            loggedins--;
                            sm.logoutSession(httpSession.getId());
                            log.info("User [{}] logged in with session [{}] has been kicked out", username, httpSession.getId());
                        }
                    } else {
                        logSubjectError(errorMsg, request, new CredentialException(errorMsg));
                        break;
                    }
                }

                if (request.getSession(false) != null) {
                    request.getSession().invalidate();
                }
                MgnlContext.login(subject);
                AuditLoggingUtil.log(loginResult, request);

                // Send response via redirect to follow PRG (Post/Redirect/Get) pattern to prevent logging in via Back button after logout. http://en.wikipedia.org/wiki/Post/Redirect/Get
                // Applied only in case authentication is done with POST request.
                if (loginResult.getStatus() == LoginResult.STATUS_SUCCEEDED_REDIRECT_REQUIRED) {
                    String location = request.getParameter(FormLogin.PARAMETER_RETURN_TO);
                    // Fallback to current request uri if no FormLogin.PARAMETER_RETURN_TO was specified
                    if (location == null) {
                        location = request.getRequestURL().toString();
                    }
                    location = RequestDispatchUtil.REDIRECT_PREFIX + location;
                    RequestDispatchUtil.dispatch(location,request,response);
                    // set new session logged in
                    loggedins += 1;
                    sm.registerUser(username, request.getSession());
                    logNewSession(loggedins, username, request.getSession());
                    return;
                }
                sm.registerUser(username, request.getSession());
                break;
            } else {
                // just log.
                AuditLoggingUtil.log(loginResult, request);
            }

        }
        // continue even if all login handlers failed
        chain.doFilter(request, response);
    }

    private void logSubjectError(String errorMsg, HttpServletRequest request, LoginException loginException) {
        LoginResult nullSubjectResult = new LoginResult(LoginResult.STATUS_FAILED, loginException);
        LoginResult.setCurrentLoginResult(nullSubjectResult);
        AuditLoggingUtil.log(nullSubjectResult, request);
        log.error(errorMsg);
    }

    private void logNewSession(int loggedins, String username, HttpSession session) throws UnknownHostException {
        String loginUserFromIP = "User [" + username + "] from '" +  InetAddress.getLocalHost() + "'";
        log.info(loginUserFromIP + " logged in #{} with session [{}]", loggedins, session.getId());
    }

    public Collection<LoginHandler> getLoginHandlers() {
        return loginHandlers;
    }

    public void setLoginHandlers(Collection<LoginHandler> loginHandlers) {
        this.loginHandlers = loginHandlers;
    }

    public void addLoginHandlers(LoginHandler handler) {
        this.loginHandlers.add(handler);
    }

    public int getMaxSessions() {
        return maxSessions;
    }

    public void setMaxSessions(int maxSessions) {
        this.maxSessions = maxSessions;
    }

    public boolean isKickOutLoggedUser() {
        return kickOutLoggedUser;
    }

    public void setKickOutLoggedUser(boolean kickOutLoggedUser) {
        this.kickOutLoggedUser = kickOutLoggedUser;
    }
}
