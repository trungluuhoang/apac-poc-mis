/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.cms.security.auth.login;

import info.magnolia.cms.managers.DefaultSessionManager;
import info.magnolia.cms.security.Realm;
import info.magnolia.cms.security.SecuritySupport;
import info.magnolia.cms.security.User;
import info.magnolia.cms.security.UserManager;
import info.magnolia.cms.util.RequestDispatchUtil;
import info.magnolia.context.Context;
import info.magnolia.context.MgnlContext;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.SneakyThrows;

/**
 * This class will handle the user's available.
 */
public class UserAvailableHandler implements LoginHandler {

    private static final Logger log = LoggerFactory.getLogger(UserAvailableHandler.class);

    private Provider<Context> contextProvider;
    private SecuritySupport securitySupport;
    @Inject
    DefaultSessionManager sm;

    @Inject
    public UserAvailableHandler(Provider<Context> contextProvider, SecuritySupport securitySupport) {
        this.contextProvider = contextProvider;
        this.securitySupport = securitySupport;
    }

    @SneakyThrows
    @Override
    public LoginResult handle(HttpServletRequest request, HttpServletResponse response) {
        UserManager userManager = securitySupport.getUserManager(Realm.REALM_ALL.getName());
        User userInSysCxt = contextProvider.get().getUser();
        if (userInSysCxt != null) {
            User user = userManager.getUser(userInSysCxt.getName());
            if (user == null) {
                log.debug("User '" + MgnlContext.getUser().getName() + "' is deleted by admin.");
                logOutCurrentUsr(MgnlContext.getUser().getName());
                return statusNoLogin(request, response);
            } else if (!user.isEnabled()) {
                log.debug("User '" + user.getName() + "' is disabled by admin.");
                logOutCurrentUsr(user.getName());
                return statusNoLogin(request, response);
            }
        }
        return LoginResult.NOT_HANDLED;
    }

    private void logOutCurrentUsr(String username) {
        List<HttpSession> copySessions = sm.getManagedSessions(username).stream().collect(Collectors.toList());
        for (HttpSession httpSession : copySessions) {
            sm.logoutSession(httpSession.getId());
        }
    }

    private LoginResult statusNoLogin(HttpServletRequest request, HttpServletResponse response) {
        RequestDispatchUtil.dispatch(RequestDispatchUtil.PERMANENT_PREFIX + "/.magnolia/admincentral/?mgnlLogout=true", request, response);
        return new LoginResult(LoginResult.STATUS_NO_LOGIN);
    }
}
