/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.cms.managers;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Singleton;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.map.MultiValueMap;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default session manager.
 */
@Singleton
public class DefaultSessionManager {
    
    private static final Logger log = LoggerFactory.getLogger(DefaultSessionManager.class);
    
    private Map<String, HttpSession> allSessions = new HashMap<>(); // jsessionid, session
    private Map<String, String> userSessions = new HashMap<>(); // jsessionid, username
    private MultiValueMap managedSessions = new MultiValueMap(); // username, session
    
    public static DefaultSessionManager instance;
    
    public DefaultSessionManager() {
        DefaultSessionManager.instance = this;
    }
    
    public boolean sessionCreated(HttpSession jsession) {
        allSessions.put(jsession.getId(), jsession);
        log.debug("Session created {}", jsession.getId());
        return true;
    }
    
    public boolean registerUser(String username, HttpSession jsession) {
        return this.userSessions.put(jsession.getId(), username) == null && this.managedSessions.put(username, jsession) == null;
    }
    
    public boolean sessionDestroyed(String jsessionid) {
        if (allSessions.containsKey(jsessionid)) {
            HttpSession s = allSessions.get(jsessionid);
            String username = whois(jsessionid);
            if (StringUtils.isNotBlank(username)) {
                if (allSessions.remove(jsessionid) != null && managedSessions.remove(username, s) != null && userSessions.remove(jsessionid) != null) {
                    log.debug("User {} logged out from session {}.", username, jsessionid);
                    return true;
                } else {
                    return false;
                }
            }
        }
        log.debug("Cannot logout session {} - session does not exist.", jsessionid);
        return false;
    }

    @SuppressWarnings("unchecked")
    public Collection<HttpSession> getManagedSessions(String user) {
        return managedSessions.getCollection(user);
    }

    public boolean logoutSession(String jsessionid) {
        if (allSessions.containsKey(jsessionid)) {
            allSessions.get(jsessionid).invalidate();
            return true;
        } else {
            return false;
        }
    }
    
    public Map<String, HttpSession> getAllSessions() {
        return allSessions;
    }
    
    public int loggedins(String username) {
        return this.managedSessions.size(username);
    }

    public String whois(String jsessionid) {
        return userSessions.get(jsessionid);
    }

}
