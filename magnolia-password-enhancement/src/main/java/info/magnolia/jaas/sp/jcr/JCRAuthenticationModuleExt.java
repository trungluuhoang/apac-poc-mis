/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.jaas.sp.jcr;

import info.magnolia.passwordenhancement.PasswordEnhancementModule;
import info.magnolia.cms.security.SecuritySupport;
import info.magnolia.cms.security.UserManager;
import info.magnolia.objectfactory.Components;
import info.magnolia.ui.api.message.Message;
import info.magnolia.ui.api.message.MessageType;
import info.magnolia.ui.framework.message.MessagesManager;

import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.security.auth.login.AccountLockedException;
import javax.security.auth.login.AccountNotFoundException;
import javax.security.auth.login.CredentialExpiredException;
import javax.security.auth.login.LoginException;

import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.util.ISO8601;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Extended Authentication module implementation using JCR to retrieve the users.
 */
public class JCRAuthenticationModuleExt extends JCRAuthenticationModule {

    private static final Logger log = LoggerFactory.getLogger(JCRAuthenticationModuleExt.class);
    private static final String PROPERTY_LAST_PASSWORD_MODIFY_DATE = "lastPasswordModifyDate";
    private final PasswordEnhancementModule passwordEnhancementModule;
    private final MessagesManager messagesManager;

    public JCRAuthenticationModuleExt() {
        this.passwordEnhancementModule = Components.getComponent(PasswordEnhancementModule.class);
        this.messagesManager = Components.getComponent(MessagesManager.class);
    }

    /**
     * /**
     * Checks is the credentials exist in the repository.
     * Checks is the password expire then send notification to user.
     *
     * @throws LoginException or specific subclasses (which will be handled further for user feedback)
     */
    @Override
    public void validateUser() throws LoginException {
        initUser();
        if (this.user == null) {
            throw new AccountNotFoundException("User account " + this.name + " not found.");
        }

        if (!this.user.isEnabled()) {
            throw new AccountLockedException("User account " + this.name + " is locked.");
        }

        if (this.isPasswordExpired()) {
            log.debug("The password of User account {} is expired.", this.name);
            throw new CredentialExpiredException("The password of User account " + this.name + " is expired.");
        }

        matchPassword();

        if (!UserManager.ANONYMOUS_USER.equals(user.getName())) {
            // update last access date for all non anonymous users
            getUserManager().updateLastAccessTimestamp(user);

            if (this.showPasswordExpireWarning()) {
                // TODO: Try to show warning message.
                // Notification.show("Your password will be expired soon. Please check the Notification for more detail!", Notification.Type.WARNING_MESSAGE);
                Message passwordExpireWarning = new Message(MessageType.WARNING, String.format("The password of User account [%s] is close to expiring!", this.user.getName()), String.format("Please change your password soon, it will expire in %s Day(s).", this.getDayToExpiring()));
                passwordExpireWarning.setSender(this.user.getName());
                messagesManager.sendMessage(this.user.getName(), passwordExpireWarning);
            }
        }
    }

    private UserManager getUserManager() {
        // can't get the factory upfront and can't use IoC as this class is instantiated by JCR/JAAS before anything else is ready.
        log.debug("getting user manager for realm {}", realm.getName());
        return SecuritySupport.Factory.getInstance().getUserManager(realm.getName());
    }


    private boolean showPasswordExpireWarning() {
        if (!passwordEnhancementModule.getEnable() || UserManager.SYSTEM_USER.equals(user.getName())) {
            return false;
        }
        return this.getDayToExpiring() <= passwordEnhancementModule.getExpireAlert();
    }

    private boolean isPasswordExpired() {
        if (!passwordEnhancementModule.getEnable() || UserManager.SYSTEM_USER.equals(user.getName())) {
            return false;
        }
        return this.getDayToExpiring() <= 0;
    }

    private long getDayToExpiring() {
        if (StringUtils.isAllEmpty(this.user.getProperty(PROPERTY_LAST_PASSWORD_MODIFY_DATE))) {
            return 0;
        } else {
            Calendar currentTime = new GregorianCalendar(TimeZone.getDefault());
            Calendar lastPasswordModifyDate = ISO8601.parse(this.user.getProperty(PROPERTY_LAST_PASSWORD_MODIFY_DATE));
            lastPasswordModifyDate.add(Calendar.DAY_OF_MONTH, passwordEnhancementModule.getMaxPasswordAge());
            return ChronoUnit.DAYS.between(currentTime.toInstant(), lastPasswordModifyDate.toInstant());
        }
    }
}
