/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.passwordenhancement.setup;

import info.magnolia.cms.filters.ManagedLoginFilter;
import info.magnolia.module.DefaultModuleVersionHandler;
import info.magnolia.module.InstallContext;
import info.magnolia.module.delta.BootstrapSingleResourceAndOrderBefore;
import info.magnolia.module.delta.SetPropertyTask;
import info.magnolia.module.delta.Task;
import info.magnolia.repository.RepositoryConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is optional and lets you manage the versions of your module,
 * by registering "deltas" to maintain the module's configuration, or other type of content.
 * If you don't need this, simply remove the reference to this class in the module descriptor xml.
 *
 * @see DefaultModuleVersionHandler
 * @see info.magnolia.module.ModuleVersionHandler
 * @see Task
 */
public class PasswordManagementVersionHandler extends DefaultModuleVersionHandler {

    /**
     * This method change permission for anonymous in public instance.
     */
    @Override
    protected List<Task> getExtraInstallTasks(InstallContext ctx) {
        List<Task> tasks = new ArrayList<Task>();
        tasks.add(new BootstrapSingleResourceAndOrderBefore("Add user available handler.", "", "/mgnl-bootstrap/magnolia-password-enhancement/config.server.filters.login.loginHandlers.Available.yaml", "Form"));
        tasks.add(new SetPropertyTask(RepositoryConstants.CONFIG, "/server/filters/login", "class", ManagedLoginFilter.class.getName()));
        return tasks;
    }

    private Task createAuthorTasks() {
        return null;
    }

    private Task createPublicTasks() {
        return null;
    }
}
