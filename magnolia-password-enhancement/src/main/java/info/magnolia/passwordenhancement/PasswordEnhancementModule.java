/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.passwordenhancement;

import info.magnolia.cms.listeners.DefaultSessionListener;
import info.magnolia.module.ModuleLifecycle;
import info.magnolia.module.ModuleLifecycleContext;
import info.magnolia.ui.configs.KnownPassDictionaries;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Password enhancement module class.
 */
public class PasswordEnhancementModule implements ModuleLifecycle {

    private static final Logger log = LoggerFactory.getLogger(PasswordEnhancementModule.class);

    private  Boolean enable;

    private Integer maxPasswordAge;

    private Integer expireAlert;

    private List<KnownPassDictionaries> passDictionaries = new ArrayList<>();
    
    @Override
    public void start(ModuleLifecycleContext moduleLifecycleContext) {
        if (log.isDebugEnabled()) {
            log.info("Module started! Remember to add {} to your web.xml section listener as a listener-class.", DefaultSessionListener.class.getName()) ;
        }
    }

    @Override
    public void stop(ModuleLifecycleContext moduleLifecycleContext) {
        if (log.isDebugEnabled()) {
            log.info("Module stopped!");}
        }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public Integer getMaxPasswordAge() {
        return maxPasswordAge;
    }

    public void setMaxPasswordAge(Integer maxPasswordAge) {
        this.maxPasswordAge = maxPasswordAge;
    }

    public Integer getExpireAlert() {
        return expireAlert;
    }

    public void setExpireAlert(Integer expireAlert) {
        this.expireAlert = expireAlert;
    }

    public List<KnownPassDictionaries> getPassDictionaries() {
        return passDictionaries;
    }

    public void setPassDictionaries(List<KnownPassDictionaries> passDictionaries) {
        this.passDictionaries = passDictionaries;
    }
}
