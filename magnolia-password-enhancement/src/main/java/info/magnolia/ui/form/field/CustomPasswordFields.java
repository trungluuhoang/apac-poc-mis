/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.ui.form.field;

import info.magnolia.cms.security.MgnlUserManager;
import info.magnolia.cms.security.SecurityUtil;
import info.magnolia.context.Context;
import info.magnolia.context.MgnlContext;

import java.net.InetAddress;

import javax.inject.Inject;
import javax.inject.Provider;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.ui.form.field.definition.CustomPasswordFieldDefinition;
import info.magnolia.ui.vaadin.integration.jcr.JcrNewNodeAdapter;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;

import com.vaadin.ui.Component;
import com.vaadin.v7.data.Item;
import com.vaadin.v7.data.Validator;
import com.vaadin.v7.ui.CustomField;
import com.vaadin.v7.ui.Label;
import com.vaadin.v7.ui.PasswordField;
import com.vaadin.v7.ui.VerticalLayout;

import lombok.SneakyThrows;

/**
 * Custom to add new feature for PasswordFields.
 */
public class CustomPasswordFields extends CustomField<String> {

    private static final Logger log = LoggerFactory.getLogger(CustomPasswordFields.class);

    private PasswordField passwordField;
    private PasswordField verificationField;
    private PasswordField currentPasswordField;
    private Provider<Context> contextProvider;
    private CustomPasswordFieldDefinition definition;
    private VerticalLayout layout;
    private boolean isLogChanges;
    private Item itemToEdit;

    @Inject
    public CustomPasswordFields(Provider<Context> contextProvider, CustomPasswordFieldDefinition definition, Item itemToEdit) {
        this.contextProvider = contextProvider;
        this.definition = definition;
        this.itemToEdit = itemToEdit;
        isLogChanges = false;
        passwordField = new PasswordField();
        passwordField.setNullRepresentation("");
        passwordField.setWidth("100%");
        if (this.definition.isVerification()) {
            verificationField = new PasswordField();
            verificationField.setNullRepresentation("");
            verificationField.setWidth("100%");
        }
        if (this.definition.isVerificationCurrentPassword()) {
            currentPasswordField = new PasswordField();
            currentPasswordField.setNullRepresentation("");
            currentPasswordField.setWidth("100%");
        }

        getContent();
    }

    @Override
    protected Component initContent() {
        // Init layout
        layout = new VerticalLayout();
        if (definition.isVerificationCurrentPassword()) {
            layout.addComponent(new Label(definition.getVerificationCurrentPasswordMessage()));
            layout.addComponent(currentPasswordField);
        }
        // add inner label to passwordField only when currentPasswordField or verificationField are used because otherwise field label is enough
        if (definition.isVerification() || definition.isVerificationCurrentPassword()) {
            layout.addComponent(new Label(definition.getPasswordMessage()));
        }
        layout.addComponent(passwordField);
        if (definition.isVerification()) {
            layout.addComponent(new Label(definition.getVerificationMessage()));
            layout.addComponent(verificationField);
        }
        return layout;
    }

    public VerticalLayout getVerticalLayout() {
        return this.layout;
    }

    /**
     * Check if both fields are equals.
     */
    @Override
    public void validate() throws Validator.InvalidValueException {
        super.validate();
        // if there is a validationField and user put value to it, then validate it to show unmatched warning.
        boolean validationFieldHasValue = verificationField != null && isNotBlankWithSpaceAllow(verificationField.getValue());
        if (isNotBlankWithSpaceAllow(passwordField.getValue()) || validationFieldHasValue) {
            if (!isLogChanges) {
                logPassChanges(currentPasswordField);
            }
            if (definition.isVerificationCurrentPassword()) {
                if (isBlankWithSpaceAllow(currentPasswordField.getValue()) || !SecurityUtil.matchBCrypted(currentPasswordField.getValue(), getCurrentLoginUserPassword())) {
                    throw new Validator.InvalidValueException(definition.getVerificationCurrentPasswordErrorMessage());
                }
                if (!definition.isAllowCurrent()) {
                    if (passwordField.getValue().equals(currentPasswordField.getValue())) {
                        throw new Validator.InvalidValueException(definition.getUseCurrentErrorMessage());
                    }
                }
            }
            if (definition.isVerification()) {
                if (isBlankWithSpaceAllow(passwordField.getValue()) || isBlankWithSpaceAllow(verificationField.getValue())) {
                    throw new Validator.InvalidValueException(definition.getVerificationErrorMessage());
                }
                if (!passwordField.getValue().equals(verificationField.getValue())) {
                    throw new Validator.InvalidValueException(definition.getVerificationErrorMessage());
                }
            }
            setInvalidCommitted(true);
            setValue(passwordField.getValue(), true);
        }
    }

    private String getCurrentLoginUserPassword() {
        String encodedPassword = contextProvider.get().getUser().getPassword();
        if (itemToEdit instanceof JcrNodeAdapter) {
            encodedPassword = PropertyUtil.getString(((JcrNodeAdapter) itemToEdit).getJcrItem(), MgnlUserManager.PROPERTY_PASSWORD, "");
        }
        return encodedPassword;
    }

    @SneakyThrows
    private void logPassChanges(PasswordField oldPass) {
        InetAddress ip = InetAddress.getLocalHost();
        String loginUserFromIP = "User {" + MgnlContext.getUser().getName() + "} from '" + ip + "'";
        if (oldPass == null) {
            if (itemToEdit instanceof JcrNewNodeAdapter) {
                log.warn(loginUserFromIP + " is trying to create a new user {" + ((JcrNewNodeAdapter) itemToEdit).getJcrItem().getName() + "}.");
            } else if (itemToEdit instanceof JcrNodeAdapter) {
                log.warn(loginUserFromIP + " is trying to edit the information of user {" + ((JcrNodeAdapter) itemToEdit).getJcrItem().getName() + "}.");
            }
        } else {
            log.warn(loginUserFromIP + " is trying to change the password.");
        }
        isLogChanges = true;
    }

    @Override
    public Class<String> getType() {
        return String.class;
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        super.setReadOnly(readOnly);
        passwordField.setReadOnly(readOnly);
        if (verificationField != null) {
            verificationField.setReadOnly(readOnly);
        }
    }

    @Override
    public boolean isEmpty() {
        // this method is used to evaluate if field contains non empty value for validation of required field
        String currentFieldValue = firstNonBlankWithSpaceAllow(passwordField.getValue(), getInternalValue());
        return isBlankWithSpaceAllow(currentFieldValue);
    }

    private boolean isNotBlankWithSpaceAllow(String value) {
        return definition.isAllowSpace() ? StringUtils.isNotEmpty(value) : StringUtils.isNotBlank(value);
    }

    private boolean isBlankWithSpaceAllow(String value) {
        return definition.isAllowSpace() ? StringUtils.isEmpty(value) : StringUtils.isNotEmpty(value);
    }

    private String firstNonBlankWithSpaceAllow(String passValue, String internalValue) {
        return definition.isAllowSpace() ? StringUtils.firstNonEmpty(passValue, internalValue)
                : StringUtils.firstNonBlank(passValue, internalValue);
    }
}
