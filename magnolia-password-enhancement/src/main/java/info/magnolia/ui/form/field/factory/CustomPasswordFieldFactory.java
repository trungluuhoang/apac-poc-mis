/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.ui.form.field.factory;

import info.magnolia.context.Context;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;

import javax.inject.Inject;
import javax.inject.Provider;

import info.magnolia.ui.form.field.CustomPasswordFields;
import info.magnolia.ui.form.field.definition.CustomPasswordFieldDefinition;
import com.vaadin.v7.data.Item;
import com.vaadin.v7.ui.Field;

/**
 * Custom PasswordFieldFactory for adding more features.
 */
public class CustomPasswordFieldFactory extends PasswordFieldFactory {

    private final Provider<Context> contextProvider;
    private final CustomPasswordFieldDefinition definition;
    private Item itemToEdit;

    @Inject
    public CustomPasswordFieldFactory(CustomPasswordFieldDefinition definition,
                                      Item relatedFieldItem,
                                      UiContext uiContext,
                                      I18NAuthoringSupport i18nAuthoringSupport,
                                      Provider<Context> contextProvider,
                                      Item itemToEdit) {
        super(definition, relatedFieldItem, uiContext, i18nAuthoringSupport, contextProvider);
        this.contextProvider = contextProvider;
        this.definition = definition;
        this.itemToEdit = itemToEdit;
    }

    @Override
    protected Field<String> createFieldComponent() {
        // Create Field
        return new CustomPasswordFields(contextProvider, definition, itemToEdit);
    }

}
