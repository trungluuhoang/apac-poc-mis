/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.ui.form.field.definition;

import info.magnolia.i18nsystem.I18nText;

/**
 * Supports more features of PasswordFieldDefinition.
 */
public class CustomPasswordFieldDefinition extends PasswordFieldDefinition {

    private boolean allowSpace = true;
    private boolean allowCurrent = false;
    private String useCurrentErrorMessage;

    public boolean isAllowSpace() {
        return allowSpace;
    }

    public void setAllowSpace(boolean allowSpace) {
        this.allowSpace = allowSpace;
    }

    public boolean isAllowCurrent() {
        return allowCurrent;
    }

    public void setAllowCurrent(boolean allowCurrent) {
        this.allowCurrent = allowCurrent;
    }

    @I18nText
    public String getUseCurrentErrorMessage() {
        return useCurrentErrorMessage;
    }

    public void setUseCurrentErrorMessage(String useCurrentErrorMessage) {
        this.useCurrentErrorMessage = useCurrentErrorMessage;
    }
}
