/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.ui.contentapp.action;

import static info.magnolia.context.MgnlContext.doInSystemContext;

import info.magnolia.cms.security.JCRSessionOp;
import info.magnolia.cms.security.MgnlUserManager;
import info.magnolia.cms.security.User;
import info.magnolia.context.Context;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.repository.RepositoryConstants;
import info.magnolia.ui.admincentral.usermenu.action.SaveUserProfileDialogAction;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.form.EditorCallback;
import info.magnolia.ui.form.EditorValidator;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;

import java.util.Calendar;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.v7.data.Item;

/**
 * Save user pass dialog action.
 */
public class SaveUserProfilePassDialogAction extends SaveUserProfileDialogAction {

    private static final Logger log = LoggerFactory.getLogger(SaveUserProfilePassDialogAction.class);

    @Inject
    public SaveUserProfilePassDialogAction(Definition definition,
                                           Item item,
                                           EditorValidator validator,
                                           EditorCallback callback,
                                           Provider<Context> contextProvider) {
        super(definition, item, validator, callback, contextProvider);
    }

    @Override
    public void execute() throws ActionExecutionException {
        Node editUser = ((JcrNodeAdapter) item).getJcrItem();
        String prevPass = getPassValue(editUser);
        super.execute();
        try {
            doInSystemContext(new JCRSessionOp<User>(RepositoryConstants.USERS) {
                @Override
                public User exec(Session session) throws RepositoryException {
                    if (!StringUtils.equals(prevPass, (String) item.getItemProperty(MgnlUserManager.PROPERTY_PASSWORD).getValue())) {
                        Node user = session.getNodeByIdentifier(editUser.getIdentifier());
                        user.setProperty("lastPasswordModifyDate", Calendar.getInstance());
                        // Save
                        session.save();
                    }
                    return null;
                }
            });
        } catch (RepositoryException e) {
            log.error("An error occurs during save user profile.", e.getMessage());
        }
    }

    private String getPassValue(Node user) {
        return PropertyUtil.getString(user, MgnlUserManager.PROPERTY_PASSWORD, "");
    }
}
