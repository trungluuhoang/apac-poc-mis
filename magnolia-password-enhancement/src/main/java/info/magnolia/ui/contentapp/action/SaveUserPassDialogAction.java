/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.ui.contentapp.action;

import info.magnolia.cms.security.MgnlUserManager;
import info.magnolia.cms.security.SecuritySupport;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.security.app.dialog.action.SaveUserDialogAction;
import info.magnolia.security.app.dialog.action.SaveUserDialogActionDefinition;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.form.EditorCallback;
import info.magnolia.ui.form.EditorValidator;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;

import java.util.Calendar;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.v7.data.Item;

/**
 * Save user pass dialog action.
 */
public class SaveUserPassDialogAction extends SaveUserDialogAction {

    private static final Logger log = LoggerFactory.getLogger(SaveUserPassDialogAction.class);

    @Inject
    public SaveUserPassDialogAction(SaveUserDialogActionDefinition definition,
                                    Item item,
                                    EditorValidator validator,
                                    EditorCallback callback,
                                    SecuritySupport securitySupport) {
        super(definition, item, validator, callback, securitySupport);
    }

    @Override
    public void execute() throws ActionExecutionException {
        JcrNodeAdapter itemAdapter = (JcrNodeAdapter) item;
        String prevPass = checkAndGetPrevPass(itemAdapter);
        super.execute();
        try {
            Node user = itemAdapter.getJcrItem();
            if ((StringUtils.isEmpty(prevPass) && user.hasProperty(MgnlUserManager.PROPERTY_PASSWORD))
                    || (StringUtils.isNotEmpty(prevPass) && !StringUtils.equals(prevPass, getPassValue(user)))) {
                user.setProperty("lastPasswordModifyDate", Calendar.getInstance());
                user.getSession().save();
            }
        } catch (RepositoryException e) {
            log.error("An error occurs during save user info.", e.getMessage());
        }
    }

    private String checkAndGetPrevPass(JcrNodeAdapter itemAdapter) {
        return itemAdapter.isNew() ? null : getPassValue(itemAdapter.getJcrItem());
    }

    private String getPassValue(Node user) {
        return PropertyUtil.getString(user, MgnlUserManager.PROPERTY_PASSWORD, "");
    }
}
