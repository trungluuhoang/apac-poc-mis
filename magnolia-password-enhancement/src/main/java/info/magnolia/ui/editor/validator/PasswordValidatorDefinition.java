/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.ui.editor.validator;

import info.magnolia.ui.form.validator.definition.ConfiguredFieldValidatorDefinition;

/**
 * Defines password max/min validator.
 */
public class PasswordValidatorDefinition extends ConfiguredFieldValidatorDefinition {

    private boolean checkKnownPassword = true;

    public PasswordValidatorDefinition() {
        setFactoryClass(PasswordFieldValidatorFactory.class);
    }

    public boolean isCheckKnownPassword() {
        return checkKnownPassword;
    }

    public void setCheckKnownPassword(boolean checkKnownPassword) {
        this.checkKnownPassword = checkKnownPassword;
    }
}
