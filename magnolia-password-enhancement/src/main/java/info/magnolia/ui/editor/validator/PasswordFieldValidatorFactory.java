/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.ui.editor.validator;

import info.magnolia.passwordenhancement.PasswordEnhancementModule;
import info.magnolia.ui.form.validator.factory.AbstractFieldValidatorFactory;

import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import info.magnolia.ui.configs.KnownPassDictionaries;
import com.vaadin.v7.data.Validator;
import com.vaadin.v7.data.validator.AbstractStringValidator;

/**
 * The factory of {@link PasswordValidatorDefinition}.
 */
public class PasswordFieldValidatorFactory extends AbstractFieldValidatorFactory<PasswordValidatorDefinition> {

    private final PasswordValidatorDefinition definition;
    private final PasswordEnhancementModule passwordEnhancementModule;

    @Inject
    public PasswordFieldValidatorFactory(PasswordValidatorDefinition definition, PasswordEnhancementModule passwordEnhancementModule) {
        super(definition);
        this.definition = definition;
        this.passwordEnhancementModule = passwordEnhancementModule;
    }

    @Override
    public Validator createValidator() {
        return new KnownPasswordValidator(getI18nErrorMessage(), passwordEnhancementModule.getPassDictionaries(), definition.isCheckKnownPassword());
    }

    /**
     * Defines known password validator.
     */
    public static class KnownPasswordValidator extends AbstractStringValidator {

        private final boolean checkKnownPassword;
        private final List<KnownPassDictionaries> passDictionaries;

        /**
         * Constructs a validator for strings.
         *
         * <p>
         * Null and empty string values are always accepted. To reject empty values,
         * set the field being validated as required.
         * </p>
         *
         * @param errorMessage the message to be included in an {@link InvalidValueException}
         * (with "{0}" replaced by the value that failed validation).
         * @param passDictionaries
         */
        public KnownPasswordValidator(String errorMessage, List<KnownPassDictionaries> passDictionaries, boolean checkKnownPassword) {
            super(errorMessage);
            this.checkKnownPassword = checkKnownPassword;
            this.passDictionaries = passDictionaries;
        }

        @Override
        protected boolean isValidValue(String value) {
            return checkKnownPassword ? compareToPassDictionaries(passDictionaries, value)  : true;
        }

        private boolean compareToPassDictionaries(List<KnownPassDictionaries> passDictionaries, String value) {
            return !passDictionaries.stream().anyMatch(pass -> StringUtils.equals(pass.getValue(), value));
        }
    }
}
