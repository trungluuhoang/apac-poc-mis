/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.pages.addon.provider;

import info.magnolia.objectfactory.Components;
import info.magnolia.rendering.engine.OutputProvider;
import info.magnolia.rendering.util.FilteringAppendableWrapper;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

/**
 * A response output that uses to write data.
 */
public class ResponseOutputProvider implements OutputProvider {

    private FilteringAppendableWrapper appendable;
    private final OutputStream outputStream;
    private final PrintWriter printWriter;
    private boolean streamCalled;
    private boolean writerCalled;

    public ResponseOutputProvider(OutputStream outputStream, PrintWriter printWriter) {
        this.outputStream = outputStream;
        this.printWriter = printWriter;
        appendable = Components.newInstance(FilteringAppendableWrapper.class);
    }

    @Override
    public Appendable getAppendable() throws IOException {
        if (appendable.getWrappedAppendable() == null) {
            appendable.setWrappedAppendable(getPrivateAppendable());
        }
        return appendable;
    }

    private Appendable getPrivateAppendable() throws IOException {
        if (streamCalled) {
            throw new IOException("Can't use appendable and output stream in parallel. getOutputStream() was already called.");
        }
        writerCalled = true;
        return printWriter;
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        if (writerCalled) {
            throw new IOException("Can't use appendable and output stream in parallel. getAppendable() was already called.");
        }
        streamCalled = true;
        return outputStream;
    }

    public void setWriteEnabled(boolean writeEnabled) {
        this.appendable.setWriteEnabled(writeEnabled);
    }
}
