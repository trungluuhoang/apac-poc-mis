/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.pages.addon.action;

import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.pages.addon.PagesAddonModule;
import info.magnolia.pages.addon.configurations.HTMLMappingConfiguration;
import info.magnolia.pages.addon.provider.ResponseOutputProvider;
import info.magnolia.registry.RegistrationException;
import info.magnolia.rendering.engine.OutputProvider;
import info.magnolia.rendering.engine.RenderingEngine;
import info.magnolia.rendering.template.TemplateDefinition;
import info.magnolia.rendering.template.registry.TemplateDefinitionRegistry;
import info.magnolia.ui.ValueContext;
import info.magnolia.ui.api.action.AbstractAction;
import info.magnolia.ui.framework.util.TempFileStreamResource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Collections;

import javax.inject.Inject;
import javax.jcr.Node;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.server.Page;


/**
 * Export HTML action.
 */
public class ExportHtmlAction extends AbstractAction<ExportHtmlActionDefinition> {

    protected static final Logger log = LoggerFactory.getLogger(ExportHtmlAction.class);

    private final ValueContext<Node> valueContext;
    private final RenderingEngine renderingEngine;
    private final TemplateDefinitionRegistry templateDefinitionRegistry;
    private final PagesAddonModule pagesAddonModule;

    @Inject
    ExportHtmlAction(ExportHtmlActionDefinition definition,
                     ValueContext<Node> valueContext,
                     RenderingEngine renderingEngine,
                     TemplateDefinitionRegistry templateDefinitionRegistry,
                     PagesAddonModule pagesAddonModule) {
        super(definition);
        this.valueContext = valueContext;
        this.renderingEngine = renderingEngine;
        this.templateDefinitionRegistry = templateDefinitionRegistry;
        this.pagesAddonModule = pagesAddonModule;
    }

    @Override
    public void execute() {
        valueContext.getSingle().ifPresent(this::executeForNode);
    }

    private void executeForNode(Node node) {
        String sltPageName = "";
        OutputStream outputStream = null;
        TempFileStreamResource tempFileStreamResource = new TempFileStreamResource();
        try {
            sltPageName = node.getName();
            String renderResult = render(sltPageName, node, NodeTypes.Renderable.getTemplate(node));
            tempFileStreamResource.setTempFileName(sltPageName);
            tempFileStreamResource.setFilename(sltPageName + ".html");
            outputStream = tempFileStreamResource.getTempFileOutputStream();
            outputStream.write(renderResult.getBytes(StandardCharsets.UTF_8));

            // Opens the resource for download
            Page.getCurrent().open(tempFileStreamResource, StringUtils.EMPTY, true);
        } catch (Exception e) {
            log.error("Error occurs during export page '{}' to HTML.", sltPageName, e.getMessage());
        } finally {
            closeQuietly(outputStream);
        }
    }

    protected String render(String pageName, Node content, String templateName) {
        TemplateDefinition templateDefinition = getTemplateDefinition(templateName);
        if (templateDefinition == null) {
            return "";
        }

        StringBuilder builder = new StringBuilder();
        OutputStream outputStream = null;
        PrintWriter printWriter = null;

        try {
            TempFileStreamResource tempFileStreamResource = new TempFileStreamResource();
            tempFileStreamResource.setTempFileName(pageName + "_tempo");
            tempFileStreamResource.setFilename(pageName + "_tempo.html");
            outputStream = tempFileStreamResource.getTempFileOutputStream();

            printWriter = new PrintWriter(new OutputStreamWriter(outputStream, StandardCharsets.UTF_8));
            OutputProvider out = new ResponseOutputProvider(outputStream, printWriter);
            //Collection<AbstractRenderingListener.RenderingListenerReturnCode> listenerResults = renderingEngine.initListeners(out, response);
            //if (listenerResults.contains(AbstractRenderingListener.RenderingListenerReturnCode.NOT_FOUND)) {
            //    return false; // this will result into 404 (e.g. request for direct area rendering of a nonexisting area)
            //}
            // render
            renderingEngine.render(content, templateDefinition, Collections.<String, Object>emptyMap(), out);
            // flush all
            printWriter.flush();
            outputStream.flush();

            // read line and build html
            buildHTML(builder, pageName, tempFileStreamResource.getStream().getStream(), pagesAddonModule.getHtmlMappingConfiguration());

        } catch (Exception e) {
            log.error("An error occurs during render and build html.", e.getMessage());
        } finally {
            closeQuietly(printWriter);
            closeQuietly(outputStream);
        }

        return builder.toString();
    }

    private void buildHTML(StringBuilder builder, String pageName, InputStream inputStream, HTMLMappingConfiguration htmlConfigs) {
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String strLine;
        try {
            StringBuilder originHtml = new StringBuilder();
            while ((strLine = bufferedReader.readLine()) != null) {
                if (log.isDebugEnabled()) {
                    originHtml.append(strLine);
                    originHtml.append("\n");
                }
                replaceResourceDomain(builder, htmlConfigs, strLine);
                builder.append("\n");
            }
            log.debug("Origin HTML of page '{}' as below: \n {}", pageName, originHtml);
            log.debug("Replaced HTML of page '{}' as below: \n {}", pageName, builder);
        } catch (IOException e) {
            log.error("An error occurs during read and replace original html.", e.getMessage());
        } finally {
            IOUtils.closeQuietly(bufferedReader);
            IOUtils.closeQuietly(inputStreamReader);
            IOUtils.closeQuietly(inputStream);
        }
    }

    private void replaceResourceDomain(StringBuilder builder, HTMLMappingConfiguration htmlConfigs, String strLine) {
        String domainName = getDomainNameFromConfigs(htmlConfigs);
        if (StringUtils.isNotEmpty(domainName)
                && !"your_public_domain".equalsIgnoreCase(domainName)) {
            if (StringUtils.isNotEmpty(MgnlContext.getContextPath())) {
                builder.append(strLine.replaceAll(MgnlContext.getContextPath(), domainName + MgnlContext.getContextPath()));
            } else {
                findAndReplaceResourceLink(builder, domainName, strLine);
            }
        }
    }

    private void findAndReplaceResourceLink(StringBuilder builder, String domainName, String strLine) {
        if (strLine.contains("href=\"") && !strLine.contains("http")) {
            builder.append(strLine.replaceAll("href=\"", "href=\"" + domainName));
        } else if (strLine.contains("src=\"") && !strLine.contains("http")) {
            builder.append(strLine.replaceAll("src=\"", "src=\"" + domainName));
        } else if (strLine.contains("action=\"") && !strLine.contains("http")) {
            builder.append(strLine.replaceAll("action=\"", "action=\"" + domainName));
        } else if (strLine.contains("url(") && !strLine.contains("http")) {
            String url = strLine.substring(0, strLine.indexOf("url") + 4);
            builder.append(StringUtils.replace(strLine, url, url + domainName));
        } else {
            builder.append(strLine);
        }
    }

    private String getDomainNameFromConfigs(HTMLMappingConfiguration htmlConfigs) {
        if (htmlConfigs.isUseLocalhost()) {
            String port = getLocalPort();
            if (StringUtils.isNotEmpty(port)) {
                return "http://localhost:" + port;
            } else {
                return "http://localhost";
            }
        } else {
            return htmlConfigs.getDomainName();
        }
    }

    private String getLocalPort() {
        String originalURL = MgnlContext.getAggregationState().getOriginalURL();
        int idx = originalURL.indexOf("/", 7);
        if (idx > 7) {
            String sub = originalURL.substring(7, idx);
            return sub.substring(sub.indexOf(":") + 1);
        }
        return "8080";
    }

    private TemplateDefinition getTemplateDefinition(String templateName) {
        try {
            return templateDefinitionRegistry.getTemplateDefinition(templateName);
        } catch (RegistrationException e) {
            log.warn("An error occurs during get template definition '{}'", templateName, e.getMessage());
            return null;
        }
    }

    private void closeQuietly(Object closeable) {
        if (closeable != null) {
            if (closeable instanceof OutputStream) {
                IOUtils.closeQuietly((OutputStream) closeable);
            }
            if (closeable instanceof PrintWriter) {
                IOUtils.closeQuietly((PrintWriter) closeable);
            }
        }
    }
}
