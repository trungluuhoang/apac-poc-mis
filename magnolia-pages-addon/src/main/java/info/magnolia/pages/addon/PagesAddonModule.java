/**
 * This file Copyright (c) 2021 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package info.magnolia.pages.addon;

import info.magnolia.pages.addon.configurations.HTMLMappingConfiguration;

import lombok.Getter;
import lombok.Setter;

/**
 * This class is optional and represents the configuration for the magnolia-pages-addon module.
 * By exposing simple getter/setter/adder methods, this bean can be configured via content2bean
 * using the properties and node from <tt>config:/modules/magnolia-pages-addon</tt>.
 * If you don't need this, simply remove the reference to this class in the module descriptor xml.
 * See https://documentation.magnolia-cms.com/display/DOCS/Module+configuration for information about module configuration.
 */
public class PagesAddonModule {

    @Getter
    @Setter
    private HTMLMappingConfiguration htmlMappingConfiguration;

}
