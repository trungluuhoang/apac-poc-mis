# Magnolia Pages Addon module guideline

## Export to Html feature

#### Installation
* For common use with DX-Core bundle, just need to build jar file and then copy it to lib folder. E.g: Copying `magnolia-pages-addon-1.0-SNAPSHOT.jar` to `<tomcat>/webapp/ROOT/WEB-INF/lib/`
* For development use with a Webapp, just need to declare it in POM of Webapp as below
```  
<dependency>
  <groupId>info.magnolia.apac.poc</groupId>
  <artifactId>magnolia-pages-addon</artifactId>
  <version>1.0-SNAPSHOT</version>
</dependency>
```
----------------------------------------------------------------------------
#### Usage

* Preparing the settings at `magnolia-pages-addon/src/main/resources/magnolia-pages-addon/config.yaml`
```
htmlMappingConfiguration:
  domainName: your_public_domain
  useLocalhost: true
```
- [ ] domainName: Put your public domain name here.
- [ ] useLocalHost: True by default, you could set it to false when you set your public domain above
* After complete the settings, just need to open Pages app and select a page that use want to export to html.
----------------------------------------------------------------------------
#### Trouble Shooting
* Enabling log debug for investigating if there is any issue
    * Login to `Admincentral`
    * Open `Log Tools` app
    * Navigate `Log Levels` tab
    * Add Logger name **info.magnolia.pages.addon.action.ExportHtmlAction** with DEBUG level
    * Check logs in `magnolia-debug.log` at the first tab
* Updating ACL and Web access for **anonymous** in author for testing purpose
* Bypass CORS in some cases
----------------------------------------------------------------------------